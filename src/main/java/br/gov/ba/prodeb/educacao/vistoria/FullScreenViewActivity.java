package br.gov.ba.prodeb.educacao.vistoria;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;

import br.gov.ba.prodeb.educacao.vistoria.adapter.FullScreenImageAdapter;
import br.gov.ba.prodeb.educacao.vistoria.helper.Utils;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

@EActivity
public class FullScreenViewActivity extends Activity{

	@Bean
	Utils utils;
	
	private FullScreenImageAdapter adapter;
	private ViewPager viewPager;
	
	@Bean
	VistoriaAtual vistoriaAtual;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fullscreen_view);

		viewPager = (ViewPager) findViewById(R.id.pager);

		Intent i = getIntent();
		int position = i.getIntExtra("position", 0);

		adapter = new FullScreenImageAdapter(FullScreenViewActivity.this,
				utils.getFilePaths(), vistoriaAtual);

		viewPager.setAdapter(adapter);

		// displaying selected image first
		viewPager.setCurrentItem(position);
	}
}
