package br.gov.ba.prodeb.educacao.vistoria.adapter;

import java.io.File;
import java.util.ArrayList;

import br.gov.ba.prodeb.educacao.vistoria.R;
import br.gov.ba.prodeb.educacao.vistoria.VistoriaAtual;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.FotoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;
import br.gov.ba.prodeb.educacao.vistoria.helper.TouchImageView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
 
public class FullScreenImageAdapter extends PagerAdapter {
 
    private Activity _activity;
    private ArrayList<String> _imagePaths;
    private LayoutInflater inflater;
    private VistoriaAtual _vistoriaAtual;
 
    // constructor
    public FullScreenImageAdapter(Activity activity,
            ArrayList<String> imagePaths, VistoriaAtual vistoriaAtual) {
        this._activity = activity;
        this._imagePaths = imagePaths;
        this._vistoriaAtual = vistoriaAtual;
    }
 
    @Override
    public int getCount() {
        return this._imagePaths.size();
    }
 
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }
    
    private String getImagePath(int position) {
    	return _imagePaths.get(position);
    }
    
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
    	TouchImageView imgDisplay;
        Button btnClose;
        Button btnApagar;
        EditText comentario;
  
        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                false);
  
        imgDisplay = (TouchImageView) viewLayout.findViewById(R.id.imgDisplay);
        btnClose = (Button) viewLayout.findViewById(R.id.btnClose);
        btnApagar = (Button) viewLayout.findViewById(R.id.btnApagar);
        comentario = (EditText) viewLayout.findViewById(R.id.comentario);
        comentario.setText(_vistoriaAtual.comentariosFotos.get(position));
        comentario.addTextChangedListener(new TextWatcher(){
	        public void afterTextChanged(Editable s) {
                	_vistoriaAtual.comentariosFotos.set(position, s.toString());

	        }
	        public void beforeTextChanged(CharSequence s, int start, int count, int after){}
	        public void onTextChanged(CharSequence s, int start, int before, int count){
	        }
        });

        Display display = _activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inPreferredConfig = Bitmap.Config.RGB_565;
//        options.inDither = true;
//        Bitmap bitmap = BitmapFactory.decodeFile(_imagePaths.get(position), options);
//        imgDisplay.setImageBitmap(Bitmap.createScaledBitmap(bitmap, size.x, size.y, false) );
        imgDisplay.setImageBitmap(lessResolution(_imagePaths.get(position), size.x, size.y));
        final String filePath = _imagePaths.get(position); 
        
        btnApagar.setOnClickListener(new View.OnClickListener() {            
            @Override
            public void onClick(View v) {
            	new AlertDialog.Builder(_activity)
        		.setIcon(R.drawable.ic_launcher)
        		.setTitle("REMOVER")
        		.setMessage("Confirma?")
        		.setCancelable(false)
        		.setNegativeButton("Não", null)
        		.setPositiveButton("Sim",
        			new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
            			_activity.getContentResolver().delete(FotoContract.CONTENT_URI, FotoContract.URI + "=?", new String[]{getImagePath(position)});
                    	File file = new File(filePath);
                    	file.delete();
            		    Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_MOUNTED");
            			File f = new File(filePath);
            		    Uri contentUri = Uri.fromFile(f);
            		    mediaScanIntent.setData(contentUri);
            		    _activity.sendBroadcast(mediaScanIntent);
                    	_vistoriaAtual.fotos.remove(position);
                    	_vistoriaAtual.comentariosFotos.remove(position);
                    	_vistoriaAtual.salvaVistoria();
                    	Intent returnIntent = new Intent();
                    	_activity.setResult(1, returnIntent);        
                        _activity.finish();
                    }
                })
        		.show();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {            
            @Override
            public void onClick(View v) {
            	_vistoriaAtual.salvaVistoria();
            	Intent returnIntent = new Intent();
            	_activity.setResult(0, returnIntent);        
                _activity.finish();
            }
        });
  
        ((ViewPager) container).addView(viewLayout);
  
        return viewLayout;
    }
     
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
  
    }
    
    public Bitmap lessResolution (String filePath, int width, int height)
    {   int reqHeight=width;
        int reqWidth=height;
         BitmapFactory.Options options = new BitmapFactory.Options();   

            // First decode with inJustDecodeBounds=true to check dimensions
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;        

            return BitmapFactory.decodeFile(filePath, options);
    }

  private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {

        // Calculate ratios of height and width to requested height and width
        final int heightRatio = Math.round((float) height / (float) reqHeight);
        final int widthRatio = Math.round((float) width / (float) reqWidth);

        // Choose the smallest ratio as inSampleSize value, this will guarantee
        // a final image with both dimensions larger than or equal to the
        // requested height and width.
        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
    }
    return inSampleSize;
    }

}