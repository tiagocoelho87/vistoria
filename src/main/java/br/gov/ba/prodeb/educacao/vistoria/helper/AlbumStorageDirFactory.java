package br.gov.ba.prodeb.educacao.vistoria.helper;

import java.io.File;

public abstract class AlbumStorageDirFactory {
	public abstract File getAlbumStorageDir(String albumName);
}
