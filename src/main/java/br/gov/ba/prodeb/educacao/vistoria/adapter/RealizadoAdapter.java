package br.gov.ba.prodeb.educacao.vistoria.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import br.gov.ba.prodeb.educacao.vistoria.adapter.RealizadoItemView_;
import br.gov.ba.prodeb.educacao.vistoria.VistoriaAtual;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.PrevistoContract;
import br.gov.ba.prodeb.educacao.vistoria.helper.DecimalDigitsInputFilter;
import br.gov.ba.prodeb.educacao.vistoria.helper.InputFilterMinMax;
import br.gov.ba.prodeb.educacao.vistoria.model.Realizado;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

@EBean
public class RealizadoAdapter extends BaseAdapter {
	
	public List<String> servicos = new ArrayList<String>();
	
	@Bean
	VistoriaAtual vistoriaAtual;

    @RootContext
    Context context;
    
	public void initAdapter(Cursor c) {
		servicos.clear();
//		valores.clear();
		while (c.moveToNext())
		{
			servicos.add(c.getString(c.getColumnIndex(PrevistoContract.DESCRICAO))); 
//			valores.add(c.getFloat(c.getColumnIndex(PrevistoContract.VALOR)));
		}
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		final RealizadoItemView realizadoItemView;
		
        if (convertView == null) {
        	realizadoItemView = RealizadoItemView_.build(context);
        } else {
        	realizadoItemView = (RealizadoItemView) convertView;
        }
        
        realizadoItemView.executado.setFilters(new InputFilter[]{ new InputFilterMinMax("0.00", "100.00"), new DecimalDigitsInputFilter(3,2)});
//        realizadoItemView.executado.setHint(String.valueOf(vistoriaAtual.realizadoAnterior.get(position)));
        
        realizadoItemView.bind(getItem(position), position);
                
        return realizadoItemView;
	}

	@Override
	public int getCount() {
			return servicos.size();
	}

	@Override
	public Realizado getItem(int position) {
		Realizado realizado = new Realizado(vistoriaAtual.servicos.get(position),vistoriaAtual.realizado.get(position),null, vistoriaAtual.avaliacao.get(position), vistoriaAtual.comentario.get(position));
		realizado.setPrevisto(vistoriaAtual.previsto.get(position));
		realizado.setValorAnterior(vistoriaAtual.realizadoAnterior.get(position));
		return realizado;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

}
