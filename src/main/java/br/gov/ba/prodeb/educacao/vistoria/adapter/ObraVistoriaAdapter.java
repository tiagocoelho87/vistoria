package br.gov.ba.prodeb.educacao.vistoria.adapter;

import br.gov.ba.prodeb.educacao.vistoria.OnRelatorioInteractionListener;
import br.gov.ba.prodeb.educacao.vistoria.R;
import br.gov.ba.prodeb.educacao.vistoria.VistoriaApplication;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.VistoriaContract;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class ObraVistoriaAdapter extends CursorAdapter {

    private OnRelatorioInteractionListener mListener;

	public ObraVistoriaAdapter(Context context, Cursor c, int flags) {
		super(context, c, flags);
        try {
            mListener = (OnRelatorioInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnRelatorioInteractionListener");
        }
    }

	@Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // when the view will be created for first time,
        // we need to tell the adapters, how each item will look
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View retView = inflater.inflate(R.layout.relatorio_list_item, parent, false);
 
        return retView;
    }
 
    @Override
    public void bindView(View view, final Context context, final Cursor cursor) {
        TextView codigo = (TextView) view.findViewById(R.id.codigo);
        TextView descricao = (TextView) view.findViewById(R.id.descricao);
        TextView dataVisita = (TextView) view.findViewById(R.id.dataVisita);
        final String title;
		final Cursor c = context.getContentResolver().query(Uri.parse(ObraContract.CONTENT_URI + "/" + cursor.getLong(cursor.getColumnIndex(VistoriaContract.OBRA))), new String[]{ObraContract._ID, ObraContract.CODIGO, ObraContract.DESCRICAO}, null, null, null);
		c.moveToNext();
        title =  c.getString(c.getColumnIndex(ObraContract.CODIGO)) + " - " + cursor.getString(cursor.getColumnIndex(VistoriaContract.DATAVISTORIA));
        codigo.setText(c.getString(c.getColumnIndex(ObraContract.CODIGO)));
        descricao.setText(c.getString(c.getColumnIndex(ObraContract.DESCRICAO)));
        dataVisita.setText(cursor.getString(cursor.getColumnIndex(VistoriaContract.DATAVISTORIA)));
        
        final long obraId = c.getLong(c.getColumnIndex(ObraContract._ID));
        final long vistoriaId =  cursor.getLong(cursor.getColumnIndex(VistoriaContract._ID));
        
        c.close();
        
        view.setOnClickListener(new OnClickListener() {

     	   @Override
     	   public void onClick(View v) {
     		 
     		 VistoriaApplication vistoriaApp = (VistoriaApplication) context.getApplicationContext();
     		 vistoriaApp.setObraId(obraId);
     		 vistoriaApp.setVistoriaId(vistoriaId);
     		 mListener.onItemSelected(title);
     	   }
     	  });
    }
    
        
}

