package br.gov.ba.prodeb.educacao.vistoria;

/**
 * Created by georgesbaladi on 2/4/14.
 */
public interface OnRelatorioInteractionListener {
    public void onItemSelected(String subTitle);
}
