package br.gov.ba.prodeb.educacao.vistoria;

import android.os.Bundle;
import br.gov.ba.prodeb.educacao.vistoria.R;
import android.app.Activity;
import android.content.Intent;
import com.crashlytics.android.Crashlytics;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import com.parse.ParseAnalytics;
import com.google.analytics.tracking.android.EasyTracker;

@EActivity(R.layout.vistoria_list_activity)
@OptionsMenu(R.menu.vistoria_list_activity)
public class VistoriaListActivity extends Activity
{

	@App
	VistoriaApplication vistoriaApp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Crashlytics.start(this);
		//ParseAnalytics.trackAppOpened(getIntent());
	}

    @OptionsItem(R.id.vistoria_relatorios)
    void startRelatorios() {
		Intent relatoriosListIntent = new Intent(this, RelatoriosListActivity_.class);
		startActivity(relatoriosListIntent);
    }
		
    @OptionsItem(R.id.vistoria_configurar)
    void startConfigurar() {
		Intent configurarIntent = new Intent(this, ConfigActivity_.class);
		startActivity(configurarIntent);
    }
		
    @OptionsItem(R.id.vistoria_preparar)
    void startPreparar() {
    	vistoriaApp.setObraId(0);
		Intent obraListIntent = new Intent(this, ObraListActivity_.class);
		startActivity(obraListIntent);
    }
		
    @Override
    public void onStart() {
      super.onStart();
      EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
      super.onStop();
      EasyTracker.getInstance(this).activityStop(this);
    }
    
    @Override
    public void onResume() {
      super.onResume();
		getActionBar().setTitle("Realizar Vistoria++");
    }

}
