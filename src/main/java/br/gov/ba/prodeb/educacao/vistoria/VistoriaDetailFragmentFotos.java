package br.gov.ba.prodeb.educacao.vistoria;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;
import br.gov.ba.prodeb.educacao.vistoria.adapter.GridViewImageAdapter;
import br.gov.ba.prodeb.educacao.vistoria.helper.AlbumStorageDirFactory;
import br.gov.ba.prodeb.educacao.vistoria.helper.BaseAlbumDirFactory;
import br.gov.ba.prodeb.educacao.vistoria.helper.FroyoAlbumDirFactory;
import br.gov.ba.prodeb.educacao.vistoria.helper.AppConstant;
import br.gov.ba.prodeb.educacao.vistoria.helper.Utils;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.vistoria_detail_fragment_fotos)
@OptionsMenu(R.menu.camera_action)
public class VistoriaDetailFragmentFotos extends Fragment {
	
	@Bean
    Utils utils;
	
    private ArrayList<String> imagePaths = new ArrayList<String>();
    private GridViewImageAdapter adapter;
    private int columnWidth;
    
	private static final int ACTION_TAKE_PHOTO_B = 1;
	
	String mCurrentPhotoPath;
	
	int fragmentWidth;
	
	@ViewById
	GridView gridView;
	
	private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
	
	@Bean
	VistoriaAtual vistoriaAtual;

	private String getAlbumName() {
		return "Obra "+ vistoriaAtual.obra.getCodigo();
	}
	
	private File getAlbumDir() {
		File storageDir = null;

		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			
			storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

			if (storageDir != null) {
				if (! storageDir.mkdirs()) {
					if (! storageDir.exists()){
						Log.d("Vistoria", "failed to create directory");
						return null;
					}
				}
			}
			
		} else {
			Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
		}
		
		return storageDir;
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "VIST_" + timeStamp + "_";
		File albumF = getAlbumDir();
		File imageF = File.createTempFile(imageFileName, ".jpg", albumF);
		return imageF;
	}

	private File setUpPhotoFile() throws IOException {
		
		File f = createImageFile();
		mCurrentPhotoPath = f.getAbsolutePath();
		
		return f;
	}
	
	private void galleryAddPic() {
		    Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
			File f = new File(mCurrentPhotoPath);
		    Uri contentUri = Uri.fromFile(f);
		    mediaScanIntent.setData(contentUri);
		    this.getActivity().sendBroadcast(mediaScanIntent);
		    vistoriaAtual.fotos.add(mCurrentPhotoPath);
		    vistoriaAtual.comentariosFotos.add("");
		    adapter.setFilePaths(utils.getFilePaths());
			adapter.notifyDataSetChanged();
			getActivity().invalidateOptionsMenu();
			vistoriaAtual.salvaVistoria();
	}


	@AfterViews
    void configure() {
 
        getActivity().invalidateOptionsMenu();
        
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
			mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
		} else {
			mAlbumStorageDirFactory = new BaseAlbumDirFactory();
		}
 
        getView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                fragmentWidth = getView().getWidth();
                if(fragmentWidth > 0)
                {
                    getView().getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    InitilizeGridLayout();
                    // loading all image paths from SD card
                    imagePaths = utils.getFilePaths();
             
                    // Gridview adapter
                    adapter = new GridViewImageAdapter(getActivity(), imagePaths,
                            columnWidth);
             
                    // setting grid view adapter
                    gridView.setAdapter(adapter);

                    gridView.setOnItemClickListener(new OnItemClickListener() {

            			@Override
                    	public void onItemClick(AdapterView parent, View v, int position, long id) {

                    	//Toast.makeText(GridViewActivity.this, "" + position, Toast.LENGTH_SHORT).show();

                    	Intent i = new Intent(getActivity(), FullScreenViewActivity_.class);

                    	i.putExtra("position", position);

                    	startActivityForResult(i, 10);
                    	
                    	}

                    });

                }
            }

          });
         
    }
 
    private void InitilizeGridLayout() {
        Resources r = getResources();
        float padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                AppConstant.GRID_PADDING, r.getDisplayMetrics());
 
        columnWidth = (int) ((fragmentWidth - ((AppConstant.NUM_OF_COLUMNS + 1) * padding)) / AppConstant.NUM_OF_COLUMNS);
 
        gridView.setNumColumns(AppConstant.NUM_OF_COLUMNS);
        gridView.setColumnWidth(columnWidth);
        gridView.setStretchMode(GridView.NO_STRETCH);
        gridView.setPadding((int) padding, (int) padding, (int) padding,
                (int) padding);
        gridView.setHorizontalSpacing((int) padding);
        gridView.setVerticalSpacing((int) padding);
    }

    @OptionsItem(R.id.foto)
	void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		File f = null;
		
		try {
			f = setUpPhotoFile();
			mCurrentPhotoPath = f.getAbsolutePath();
			takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
		} catch (IOException e) {
			e.printStackTrace();
			f = null;
			mCurrentPhotoPath = null;
		}
		
		startActivityForResult(takePictureIntent, ACTION_TAKE_PHOTO_B);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 10) {
			if (resultCode==1) {
    		    adapter.setFilePaths(utils.getFilePaths());
    			adapter.notifyDataSetChanged();
    	        getActivity().invalidateOptionsMenu();
			}
		} else {
			if ((resultCode==-1) && (mCurrentPhotoPath != null)) {
				galleryAddPic();
			}			
			mCurrentPhotoPath = null;
		}
	}
	
	@Override
	public void onDetach() {
	    super.onDetach();
	}

	@Override
	public void onPrepareOptionsMenu (Menu menu) {
		super.onPrepareOptionsMenu(menu);
		if ((vistoriaAtual != null) && (vistoriaAtual.fotos.size() == 10)) {
			MenuItem item= menu.findItem(R.id.foto);
		    item.setVisible(false);
		}
	}
}
