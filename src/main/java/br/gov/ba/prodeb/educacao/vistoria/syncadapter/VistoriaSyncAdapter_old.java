package br.gov.ba.prodeb.educacao.vistoria.syncadapter;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.FotoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.PrevistoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.RealizadoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.VistoriaContract;


public class VistoriaSyncAdapter_old extends AbstractThreadedSyncAdapter {

    private static final String TAG = "VistoriaSyncAdapter";


    private Context mContext;

    public VistoriaSyncAdapter_old(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContext = context;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public VistoriaSyncAdapter_old(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContext = context;
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              final ContentProviderClient provider, SyncResult syncResult) {

        	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
        	String emailFiscal = settings.getString("Email", "");
        	String tipoFiscal = settings.getString("tipoFiscal", "civil");
        	final String ultimaAtualizacao = settings.getString("ultimaAtualizacao", "2001-01-01 00:00:00");
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        	Date dtUltimaAtualizacao = null;
        	try {
        	    dtUltimaAtualizacao = dateFormat.parse(ultimaAtualizacao);
        	} catch (Exception e) {
        	    // TODO Auto-generated catch block
        	    e.printStackTrace();
        	}

        	if (!(emailFiscal.isEmpty())) {
        		ParseQuery<ParseObject> query = ParseQuery.getQuery("obra");
        		query.whereGreaterThan("updatedAt", dtUltimaAtualizacao);
        		if (tipoFiscal.equals("civil")) {
            		query.whereEqualTo("emailFiscalCivil", emailFiscal);
        		} else {
            		query.whereEqualTo("emailFiscalEletrico", emailFiscal);
        		}
        		query.findInBackground(new FindCallback<ParseObject>() {
        			@Override
        			public void done(List<ParseObject> obraList, ParseException e) {
        				if (e == null) {
        					mergeLocal(provider, obraList, ultimaAtualizacao);
        				} else {
        					Log.d("obra", "Error: " + e.getMessage());
        				}
        			}
        		});
        		mergeRemote(provider, ultimaAtualizacao);
        	}

    }

    private void mergeLocal(ContentProviderClient provider, List<ParseObject> obraList, String ultimaAtualizacao) {

    	ContentValues values = new ContentValues();
        for (ParseObject localObra : obraList) {
            try {
                Cursor c = provider.query(ObraContract.CONTENT_URI, new String[]{ObraContract.CODIGO}, ObraContract.CODIGO+"=?", new String[]{String.valueOf(localObra.getNumber("idObra"))}, null);
            	values.clear();
                values.put(ObraContract.CONCLUIDA, 0);
                values.put(ObraContract.DATAINICIOOBRA, String.valueOf(localObra.getString("dataInicioObra")));
                values.put(ObraContract.FORMALIZACAO, String.valueOf(localObra.getString("formalizacao")));
                values.put(ObraContract.IDFORMALIZACAO, String.valueOf(localObra.getString("idFormalizacao")));
                values.put(ObraContract.MODALIDADE, String.valueOf(localObra.getString("modalidade")));
                values.put(ObraContract.IDMODALIDADE, String.valueOf(localObra.getString("idModalidade")));
                values.put(ObraContract.MUNICIPIO, String.valueOf(localObra.getString("municipio")));
                values.put(ObraContract.OBJETO, String.valueOf(localObra.getString("objeto")));
                values.put(ObraContract.TIPOVISTORIA, String.valueOf(localObra.getString("tipoVistoria")));
                values.put(ObraContract.UEE, String.valueOf(localObra.getString("uee")));
                values.put(ObraContract.VALOROBRA, String.valueOf(localObra.getNumber("valorObra")));
                values.put(ObraContract.EMAILFISCALCIVIL, String.valueOf(localObra.getString("emailFiscalCivil")));
                values.put(ObraContract.EMAILFISCALELETRICO, String.valueOf(localObra.getString("emailFiscalEletrico")));
                values.put(ObraContract.NOMEFISCALCIVIL, String.valueOf(localObra.getString("nomeFiscalCivil")));
                values.put(ObraContract.NOMEFISCALELETRICO, String.valueOf(localObra.getString("nomeFiscalEletrico")));
                values.put(ObraContract.CREAFISCALCIVIL, String.valueOf(localObra.getString("creaFiscalCivil")));
                values.put(ObraContract.CREAFISCALELETRICO, String.valueOf(localObra.getString("creaFiscalEletrico")));
            	if (c.getCount() == 0) {
                    values.put(ObraContract.CODIGO, String.valueOf(localObra.getNumber("idObra")));
            		provider.insert(ObraContract.CONTENT_URI, values);
            	} else {
            		c.moveToNext();
            		provider.update(Uri.parse(ObraContract.CONTENT_URI + "/" + c.getLong(0)), values, null, null);
            	}
                c.close();
    		} catch (RemoteException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        }
    	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("ultimaAtualizacao", dateFormat.format(new Date()));
		editor.commit();
		Intent intent = new Intent("END_SYNC");
		LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }

    //TODO e se mudar o email?

    private void mergeRemote(ContentProviderClient provider, String ultimaAtualizacao) {
    	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
    	final String emailFiscal = settings.getString("Email", "");
    	final String tipoFiscal = settings.getString("tipoFiscal", "civil");
    	Cursor c = null;
    	try {
    		//obra
    		c = provider.query(ObraContract.CONTENT_URI, null, ObraContract.UPDATED+">?", new String[]{ultimaAtualizacao}, null);
			while (c.moveToNext()) {
	        	final String descricao = c.getString(c.getColumnIndex(ObraContract.DESCRICAO));
        		ParseQuery<ParseObject> query = ParseQuery.getQuery("obra");
        		query.whereEqualTo("idObra", c.getInt(c.getColumnIndex(ObraContract.CODIGO)));
        		if (tipoFiscal.equals("civil")) {
            		query.whereEqualTo("emailFiscalCivil", emailFiscal);
        		} else {
            		query.whereEqualTo("emailFiscalEletrico", emailFiscal);
        		}
        		query.findInBackground(new FindCallback<ParseObject>() {
        			@Override
        			public void done(List<ParseObject> obraList, ParseException e) {
        				if (e == null) {
        					if (obraList.size() > 0) {
            					obraList.get(0).put("descricao", descricao);
              			      	obraList.get(0).saveInBackground();
        					}
        				} else {
        					Log.d("obra", "Error: " + e.getMessage());
        				}
        			}
        		});
			}
        	c.close();
//        	previsto
        	c = provider.query(PrevistoContract.CONTENT_URI, null, PrevistoContract.UPDATED+">?", new String[]{ultimaAtualizacao}, null);
    		while (c.moveToNext()) {
    	        long idObraPrevisto = c.getLong(c.getColumnIndex(PrevistoContract.OBRA));
    	    	Cursor d = provider.query(Uri.parse(ObraContract.CONTENT_URI + "/" + idObraPrevisto), null, null, null, null);
    	    	d.moveToNext();
    	    	final int codigoObra = Integer.parseInt(d.getString(d.getColumnIndex(ObraContract.CODIGO)));
    	    	d.close();
    	        final String descricaoPrevisto = c.getString(c.getColumnIndex(PrevistoContract.DESCRICAO));
    	        final float valorPrevisto = c.getFloat(c.getColumnIndex(PrevistoContract.VALOR));
    	        ParseQuery<ParseObject> query = ParseQuery.getQuery("previsto");
            	query.whereEqualTo("idObra", codigoObra);
            	query.whereEqualTo("descricao", descricaoPrevisto);
            	query.findInBackground(new FindCallback<ParseObject>() {
            		@Override
            		public void done(List<ParseObject> previstoList, ParseException e) {
            			if (e == null) {
            				if (previstoList.size() > 0) {
                				previstoList.get(0).put("idObra", codigoObra);
           	    				previstoList.get(0).put("descricao", descricaoPrevisto);
           	    				previstoList.get(0).put("valor", valorPrevisto);
            					previstoList.get(0).saveInBackground();
           					} else {
           	    				ParseObject previsto = new ParseObject("previsto");
           	    				previsto.put("idObra", codigoObra);
                				previsto.put("descricao", descricaoPrevisto);
           	    				previsto.put("valor", valorPrevisto);
           						previsto.saveInBackground();
           					}
            			} else {
           					Log.d("previsto", "Error: " + e.getMessage());
           				}
            		}
            	});
			}
        	c.close();
//        	vistoria
        	c = provider.query(VistoriaContract.CONTENT_URI, null, VistoriaContract.UPDATED+">?", new String[]{ultimaAtualizacao}, null);
    		while (c.moveToNext()) {
    	        final long idObraVistoria = c.getLong(c.getColumnIndex(VistoriaContract.OBRA));
    	    	Cursor d = provider.query(Uri.parse(ObraContract.CONTENT_URI + "/" + idObraVistoria), null, null, null, null);
    	    	d.moveToNext();
    	    	final int codigoObra = Integer.parseInt(d.getString(d.getColumnIndex(ObraContract.CODIGO)));
                d.close();
    	        final String dataVistoria = c.getString(c.getColumnIndex(VistoriaContract.DATAVISTORIA));
    	        final String statusObra = c.getString(c.getColumnIndex(VistoriaContract.STATUSOBRA));
    	        final float valorMedicao = c.getFloat(c.getColumnIndex(VistoriaContract.VALORMEDICAO));
    	        final String comentarios = c.getString(c.getColumnIndex(VistoriaContract.COMENTARIOS));
    	        final int planilha = c.getInt(c.getColumnIndex(VistoriaContract.PLANILHA));
    	        final int diario = c.getInt(c.getColumnIndex(VistoriaContract.DIARIO));
    	        final int especificacoes = c.getInt(c.getColumnIndex(VistoriaContract.ESPECIFICACOES));
    	        final int projetos = c.getInt(c.getColumnIndex(VistoriaContract.PROJETOS));
    	        final String inicio = c.getString(c.getColumnIndex(VistoriaContract.INICIO));
    	        final String fim = c.getString(c.getColumnIndex(VistoriaContract.FIM));
    	        final String latitude = c.getString(c.getColumnIndex(VistoriaContract.LATITUDE));
    	        final String longitude = c.getString(c.getColumnIndex(VistoriaContract.LONGITUDE));

               // final String email_Fiscal = d.getString(c.getColumnIndex(VistoriaContract.EMAILFISCAL));


                ParseQuery<ParseObject> query = ParseQuery.getQuery("vistoria");
            	query.whereEqualTo("idObra", codigoObra);
            	query.whereEqualTo("dataVistoria", dataVistoria);
            	query.findInBackground(new FindCallback<ParseObject>() {
            		@Override
            		public void done(List<ParseObject> vistoriaList, ParseException e) {
                        // throw new ApiException(500, e.getMessage());
                        //HashMap vistoria = new HashMap();
            			if (e == null) {
            				if (vistoriaList.size() > 0) {
                                //Usando HashMap temos que retirar os gets
            					vistoriaList.get(0).put("idObra", codigoObra);
            					vistoriaList.get(0).put("dataVistoria", dataVistoria);
            					vistoriaList.get(0).put("statusObra", statusObra);
            					vistoriaList.get(0).put("valor", valorMedicao);
            					vistoriaList.get(0).put("comentarios", comentarios);
                                vistoriaList.get(0).put("planilha", planilha);
            					vistoriaList.get(0).put("diario", diario);
            					vistoriaList.get(0).put("especificacoes", especificacoes);
            					vistoriaList.get(0).put("projetos", projetos);
            					vistoriaList.get(0).put("inicio", inicio);
            					vistoriaList.get(0).put("fim", fim);
           	    				if (latitude != null) {
           	    					vistoriaList.get(0).put("latitude", latitude);
           	    				}
           	    				if (longitude != null) {
           	    					vistoriaList.get(0).put("longitude", longitude);
           	    				}
                                vistoriaList.get(0).put("tipoFiscal", tipoFiscal);
                                vistoriaList.get(0).put("emailFiscal", emailFiscal);
            					vistoriaList.get(0).saveInBackground();
                                // saveinbackgroud?
           					} else {
           	    				ParseObject vistoria = new ParseObject("vistoria");
                                //HashMap vistoria = new HashMap();
           	    				vistoria.put("idObra", codigoObra);
           	    				vistoria.put("dataVistoria", dataVistoria);
           	    				vistoria.put("statusObra", statusObra);
           	    				vistoria.put("valorMedicao", valorMedicao);
           	    				vistoria.put("comentarios", comentarios);
                                vistoria.put("planilha", planilha);
           	    				vistoria.put("diario", diario);
           	    				vistoria.put("especificacoes", especificacoes);
           	    				vistoria.put("projetos", projetos);
           	    				vistoria.put("inicio", inicio);
           	    				vistoria.put("fim", fim);
           	    				if (latitude != null) {
           	    					vistoria.put("latitude", latitude);
           	    				}
           	    				if (longitude != null) {
           	    					vistoria.put("longitude", longitude);
           	    				}
                                vistoria.put("tipoFiscal", tipoFiscal);
                                vistoria.put("emailFiscal", emailFiscal);
                                vistoria.saveInBackground();
           					}
            			} else {
           					Log.d("vistoria", "Error: " + e.getMessage());
           				}
            		}
            	});
			}
        	c.close();
//        	realizado
        	c = provider.query(RealizadoContract.CONTENT_URI, null, RealizadoContract.UPDATED+">?", new String[]{ultimaAtualizacao}, null);
    		while (c.moveToNext()) {
    	        final long idVistoria = c.getLong(c.getColumnIndex(RealizadoContract.VISTORIA));
    	        final String descricao = c.getString(c.getColumnIndex(RealizadoContract.DESCRICAO));
    	        final float valor = c.getFloat(c.getColumnIndex(RealizadoContract.VALOR));
    	        final String avaliacao = c.getString(c.getColumnIndex(RealizadoContract.AVALIACAO));
    	        final String comentarios = c.getString(c.getColumnIndex(RealizadoContract.COMENTARIOS));
    	        ParseQuery<ParseObject> query = ParseQuery.getQuery("realizado");
            	query.whereEqualTo("idVistoria", idVistoria);
            	query.whereEqualTo("descricao", descricao);
            	query.findInBackground(new FindCallback<ParseObject>() {
            		@Override
            		public void done(List<ParseObject> realizadoList, ParseException e) {
            			if (e == null) {
            				if (realizadoList.size() > 0) {
            					realizadoList.get(0).put("idVistoria", idVistoria);
            					realizadoList.get(0).put("descricao", descricao);
            					realizadoList.get(0).put("valor", valor);
            					realizadoList.get(0).put("avaliacao", avaliacao);
            					realizadoList.get(0).put("comentarios", comentarios);
            					realizadoList.get(0).saveInBackground();
           					} else {
           	    				ParseObject realizado = new ParseObject("realizado");
           	    				realizado.put("idVistoria", idVistoria);
           	    				realizado.put("descricao", descricao);
           	    				realizado.put("valor", valor);
           	    				realizado.put("avaliacao", avaliacao);
           	    				realizado.put("comentarios", comentarios);
           	    				realizado.saveInBackground();
           					}
            			} else {
           					Log.d("realizado", "Error: " + e.getMessage());
           				}
            		}
            	});
			}
//        	foto
        	c = provider.query(FotoContract.CONTENT_URI, null, FotoContract.UPDATED+">?", new String[]{ultimaAtualizacao}, null);
    		while (c.moveToNext()) {
    	        final long idVistoria = c.getLong(c.getColumnIndex(FotoContract.VISTORIA));
    	        final String uri = c.getString(c.getColumnIndex(FotoContract.URI));
    	        final String comentario = c.getString(c.getColumnIndex(FotoContract.COMENTARIO));
    	        ParseQuery<ParseObject> query = ParseQuery.getQuery("fotos");
            	query.whereEqualTo("idVistoria", idVistoria);
            	query.whereEqualTo("uri", uri);
            	query.findInBackground(new FindCallback<ParseObject>() {
            		@Override
            		public void done(List<ParseObject> fotoList, ParseException e) {
            			if (e == null) {
            				if (fotoList.size() > 0) {
            					fotoList.get(0).put("idVistoria", idVistoria);
            					fotoList.get(0).put("uri", uri);
/*            					Bitmap bmp = BitmapFactory.decodeFile(uri, new BitmapFactory.Options());
            					ByteArrayOutputStream stream = new ByteArrayOutputStream();
            					bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            					byte[] byteArray = stream.toByteArray();
            					fotoList.get(0).put("foto", byteArray);
*/
            					fotoList.get(0).put("comentario", comentario);
            					fotoList.get(0).saveInBackground();        			        	        						
           					} else {
           	    				ParseObject foto = new ParseObject("fotos");
           	    				foto.put("idVistoria", idVistoria);
                				foto.put("uri", uri);
/*            					Bitmap bmp = BitmapFactory.decodeFile(uri, new BitmapFactory.Options());
            					ByteArrayOutputStream stream = new ByteArrayOutputStream();
            					bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            					byte[] byteArray = stream.toByteArray();
                				foto.put("foto", byteArray);
*/
                				foto.put("comentario", comentario);
                				foto.saveInBackground(); 
           					}
            			} else {
           					Log.d("fotos", "Error: " + e.getMessage());
           				}
            		}
            	});
			}
            c.close();

			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

    }

}
