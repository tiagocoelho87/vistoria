package br.gov.ba.prodeb.educacao.vistoria;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;


import com.google.analytics.tracking.android.EasyTracker;

import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import br.gov.ba.prodeb.educacao.vistoria.syncadapter.SyncUtils;

@EActivity(R.layout.config_activity)
public class ConfigActivity extends Activity {


    protected String dsp_url;
    protected String session_id;


    @ViewById
    EditText email;

   // @ViewById
   // EditText texto;

    @ViewById
    CheckBox sync_auto;

    @ViewById
    RadioButton civil, eletrico;

    @ViewById
    TextView ultima;

	@Click(R.id.resetSync)
	void resetSync() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        Editor editor = sharedPreferences.edit();
        editor.remove("ultimaAtualizacao");
        editor.commit();
        ultima.setText("Última atualização: reinicializado");
	}



	@Click(R.id.btnSyncNow)
    void syncNow() {
        saveConfig();
        SyncUtils.TriggerRefresh();
    }


/*
    @Click(R.id.btn2)
    void testnow2() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_launcher)
                .setTitle("ATENÇÃO")
                .setMessage("Tesssteee.")
                .setCancelable(false)
                .setNeutralButton("OK", null)
                .show();
    }
    @Click(R.id.btn1)
    void testnow3(){

        Intent intent = new Intent(ConfigActivity.this, ToDoDemoActivity.class);
        intent.putExtra("texto", texto.getText().toString());
        startActivity(intent);

    }*/

    @CheckedChange(R.id.sync_auto)
	void checkedChangedSyncCheckBox() {
		if (sync_auto.isChecked()) {
			SyncUtils.AutoSyncAccount();
		} else {
			SyncUtils.ManualSyncAccount();
	    	new AlertDialog.Builder(this)
			.setIcon(R.drawable.ic_launcher)
			.setTitle("ATENÇÃO")
			.setMessage("Ao desabilitar essa opção você fica responsável pela atualização e backup dos dados através da opção de sincronismo manual.")
			.setCancelable(false)
			.setNeutralButton("OK", null)
			.show();
		}
	}
	
	@Override
    public void onStart() {
      super.onStart();
      EasyTracker.getInstance(this).activityStart(this);
      LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
			      new IntentFilter("END_SYNC"));

    }

    @Override
    public void onStop() {
      super.onStop();
      EasyTracker.getInstance(this).activityStop(this);
      LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onResume() {
      super.onResume();
		getActionBar().setTitle("Configurar");
		loadConfig();		
		atualizaData();
    }

    @Override
    public void onPause() {
      super.onPause();
      saveConfig();
    }

    private void atualizaData() {
    	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
    	String ultimaAtualizacao = settings.getString("ultimaAtualizacao", "nenhuma");
    	ultima.setText("Última atualização: " + ultimaAtualizacao);
    }
    
    private void saveConfig() {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        Editor editor = sharedPreferences.edit();
        editor.putBoolean("SyncAuto", sync_auto.isChecked());
        editor.putString("Email", email.getText().toString());
       // editor.putString("Texto", texto.getText().toString());
        if (civil.isChecked()) {
        	editor.putString("tipoFiscal", "civil");
        } else {
        	editor.putString("tipoFiscal", "eletrico");        	
        }
        editor.commit();
    }
    
    private void loadConfig() {
		SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this);
		boolean syncAutoValue = sharedPreferences.getBoolean("SyncAuto", true);
		String emailValue = sharedPreferences.getString("Email", "");
        String TextoValue = sharedPreferences.getString("Texto", "");
		if (syncAutoValue) {
			sync_auto.setChecked(true);
		} else {
			sync_auto.setChecked(false);

		}
		email.setText(emailValue);
//        texto.setText(TextoValue);
		String tipo_fiscal = sharedPreferences.getString("tipoFiscal", "civil");
		if (tipo_fiscal.equals("civil")) {
			civil.setChecked(true);
		} else {
			eletrico.setChecked(true);
		}
    }
    
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
    	  @Override
    	  public void onReceive(Context context, Intent intent) {
    	        atualizaData();
    	  }
      };

}
