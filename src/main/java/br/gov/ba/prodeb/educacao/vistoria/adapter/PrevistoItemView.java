package br.gov.ba.prodeb.educacao.vistoria.adapter;

import br.gov.ba.prodeb.educacao.vistoria.R;
import br.gov.ba.prodeb.educacao.vistoria.R.layout;
import br.gov.ba.prodeb.educacao.vistoria.model.Previsto;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

@EViewGroup(R.layout.obra_detail_list_item)
public class PrevistoItemView extends RelativeLayout {
	
	@ViewById
	TextView servico;
	
	@ViewById
	EditText valor;

	TextWatcher valorTW;
	
	public PrevistoAdapter adapter;
	
	public int position;
    
	public PrevistoItemView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

    public void bind(Previsto previsto) {
		if (valorTW != null) {
			getValor().removeTextChangedListener(valorTW);
    	}
        servico.setText(previsto.getDescricao().toUpperCase());
        if (previsto.getValor()>0) {
        	getValor().setText(String.valueOf(previsto.getValor()));
        } else {
        	getValor().setText("");
        }
        
        getValor().setSelectAllOnFocus(true);

            valorTW = new TextWatcher(){
    	        public void afterTextChanged(Editable s) {
    				try {
                    	adapter.valores[position] = Float.parseFloat(s.toString());
    				} catch (Exception e) {
    					adapter.valores[position] = 0f;
    				}			    		

    	        }
    	        public void beforeTextChanged(CharSequence s, int start, int count, int after){}
    	        public void onTextChanged(CharSequence s, int start, int before, int count){
    	        }
            };
            
            getValor().addTextChangedListener(valorTW);

    }

	public EditText getValor() {
		return valor;
	}

	public void setValor(EditText valor) {
		this.valor = valor;
	}
    
}
