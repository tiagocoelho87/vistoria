package br.gov.ba.prodeb.educacao.vistoria.adapter;

import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.PrevistoContract;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;

public class LoaderObrasVistorias extends CursorLoader {
	
	public LoaderObrasVistorias(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	 @Override
	  public Cursor loadInBackground() {
		 
		 //TODO procurar solução melhor
		Cursor r = this.getContext().getContentResolver().query(PrevistoContract.CONTENT_URI,new String[]{PrevistoContract.OBRA}, null, null, null);
		String validos[] = new String[r.getCount()];
		int i = 0;
		String args = "(";
		while (r.moveToNext()) {
			validos[i] = r.getString(r.getColumnIndex((PrevistoContract.OBRA)));
			args = args + "?,";
			i++;
		}
		if (args.equals("(")) args = args + "0,";
		Cursor c =  this.getContext().getContentResolver().query(ObraContract.CONTENT_URI, null, ObraContract.CONCLUIDA + "=0 AND " + ObraContract._ID + " IN " + args.substring(0, args.length()-1) + ")", validos, ObraContract.CODIGO);
	    return c;
	  }
	 
}
