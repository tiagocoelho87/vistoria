package br.gov.ba.prodeb.educacao.vistoria.adapter;

import br.gov.ba.prodeb.educacao.vistoria.R;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.VistoriaContract;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class VistoriaAdapter extends CursorAdapter {

	public VistoriaAdapter(Context context, Cursor c, int flags) {
		super(context, c, flags);
	}

	@Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View retView = inflater.inflate(R.layout.vistoria_list_item, parent, false);
 
        return retView;
    }
 
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView codigo = (TextView) view.findViewById(R.id.codigo);
        codigo.setText(cursor.getString(cursor.getColumnIndex(ObraContract.CODIGO)));
 
        TextView descricao = (TextView) view.findViewById(R.id.descricao);
        descricao.setText(cursor.getString(cursor.getColumnIndex(ObraContract.DESCRICAO)));
        TextView data = (TextView) view.findViewById(R.id.dataVisita);
		Cursor c =  context.getContentResolver().query(ObraContract.CONTENT_URI, new String[] {ObraContract._ID}, ObraContract.CODIGO + "=?", new String[] {codigo.getText().toString()}, null);
		c.moveToNext();
		long obraId = c.getLong(0);
		c =  context.getContentResolver().query(VistoriaContract.CONTENT_URI, new String[] {VistoriaContract.DATAVISTORIA, VistoriaContract.CONCLUIDA}, VistoriaContract.OBRA + "=?", new String[] {String.valueOf(obraId)}, VistoriaContract.CONCLUIDA + " ASC");
		if (c.moveToNext()) {
			if (c.getInt(c.getColumnIndex(VistoriaContract.CONCLUIDA)) == 0) {
				data.setText(c.getString(c.getColumnIndex(c.getColumnName(0))));
			} else {
		        data.setText("Nova vistoria");							
			}
		} else {
	        data.setText("Nova vistoria");			
		}
		c.close();

    }
        
}

