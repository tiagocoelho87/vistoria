package br.gov.ba.prodeb.educacao.vistoria;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;

import com.df.adapters.ToDoAdapter;
import com.df.ui.BaseActivity;
import com.df.utils.IAppConstants;
import com.dreamfactory.api.DbApi;
import com.dreamfactory.api.UserApi;
import com.dreamfactory.model.FilterRecordRequest;
import com.dreamfactory.model.FilterRequest;
import com.dreamfactory.model.Login;
import com.dreamfactory.model.RecordRequest;
import com.dreamfactory.model.RecordResponse;
import com.dreamfactory.model.RecordsResponse;
import com.dreamfactory.model.Session;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by u034202 on 04/03/2015.
 */
public class ToDoDemoActivity extends BaseActivity {

    private UserApi userApi;
    private ToDoAdapter adapter = null;
    private ProgressDialog progressDialog;

   /* protected void log(String message) {
        System.out.println("log: " + message);
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        progressDialog = new ProgressDialog(ToDoDemoActivity.this);
        progressDialog.setMessage("Please wait...");

         //CreateRecordTask addToDoItem = new CreateRecordTask();   //funciona
         //addToDoItem.execute(texto);                               //funciona

        //UpdateRecordTask updateItem = new UpdateRecordTask();     //funciona
        //updateItem.execute(id, texto);                            //funciona

        //DeleteRecordTask delItem = new DeleteRecordTask();        //funciona
        //delItem.execute(id);                                      //funciona

        //GetRecordsTask getItem = new GetRecordsTask();
        //getItem.execute();

    }

    protected void caughtSession() {
        LoginTask loginTask = new LoginTask();
        loginTask.execute();
    }

    public AsyncTask getDados(String table, String sqlWhere) {
        if (session_id == null) //Verifica se sessão já foi criada
            caughtSession();
        GetRecordsTask getItem = new GetRecordsTask();
        return getItem.execute(table, sqlWhere);
    }

    public AsyncTask getDados2(String table, String sqlWhere) {
        if (session_id == null)
            caughtSession();
        GetRecordsTask getItem = new GetRecordsTask();
        return getItem.execute(table, sqlWhere, "1"); //Parametro com limite de linhas em resposta. "1"
    }

    public void createDados(String table, String dados) {
        if (session_id == null)
            caughtSession();
        CreateRecordTask addToDoItem = new CreateRecordTask();
        addToDoItem.execute(table, dados);
    }

    public void updateDados(String table, String dados, String sqlWhere) {
        if (session_id == null)
            caughtSession();
        UpdateRecordTask updateItem = new UpdateRecordTask();
        updateItem.execute(table, dados, sqlWhere);
    }

    public void deleteDados(String table, String dados) {
        if (session_id == null)
            caughtSession();
        DeleteRecordTask delToDoItem = new DeleteRecordTask();
        delToDoItem.execute(table, dados);
    }


    class LoginTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {

            userApi = new UserApi();
            userApi.addHeader("X-DreamFactory-Application-Name", IAppConstants.APP_NAME);
            userApi.setBasePath(IAppConstants.DSP_URL + IAppConstants.DSP_URL_SUFIX);

            Login login = new Login();
            login.setEmail("tiago.coelho@prodeb.ba.gov.br");
            //login.setEmail("tiago_spawn@hotmail.com");
            login.setPassword("159456"); //Colocar senha

            try {
                Session session = userApi.login(login);
                session_id = session.getSession_id();

/*              PrefUtil.putString(getApplicationContext(), IAppConstants.DSP_URL, dsp_url);
                PrefUtil.putString(getApplicationContext(), IAppConstants.SESSION_ID, session_id);
                PrefUtil.putString(getApplicationContext(), IAppConstants.EMAIL, "tiago.coelho@prodeb.ba.gov.br");
                PrefUtil.putString(getApplicationContext(), IAppConstants.EMAIL, "tiago_spawn@hotmail.com");
                PrefUtil.putString(getApplicationContext(), IAppConstants.PWD, "******");*/
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    class CreateRecordTask extends AsyncTask<String, Void, RecordResponse> {
        private String errorMsg;

        @Override
        protected void onPreExecute() {
//            progressDialog.show();
        }
        @Override
        protected RecordResponse doInBackground(String... params) {
            System.out.println("Tabela=" + params[0]);
            System.out.println("Lista com valor das variaveis=" + params[1]);

            Map lista = getLista(params[1]);

            RecordRequest record = new RecordRequest(); // Classe responsável por construir o JSON
            if (params[0] == "obra") {
                record.setIdObra((String) lista.get("idObra"));
                record.setDescricao((String) lista.get("descricao"));
                record.setEmailFiscalCivil((String) lista.get("emailFiscalCivil"));
                record.setEmailFiscalEletrico((String) lista.get("emailFiscalEletrico"));
                record.setUpdatedAt((String) lista.get("updatedAt"));
            }

            if (params[0] == "previsto") {
                record.setIdObra((String) lista.get("idObra"));
                record.setDescricao((String) lista.get("descricao"));
                record.setValor((String) lista.get("valor"));
            }

            if (params[0] == "vistoria") {
                record.setIdObra((String) lista.get("idObra"));
                record.setDataVistoria((String) lista.get("dataVistoria"));
                record.setStatusObra((String) lista.get("statusObra"));
                record.setEspecificacoes((String) lista.get("especificacoes"));
                record.setDiario((String) lista.get("diario"));
                record.setProjetos((String) lista.get("projetos"));
                record.setPlanilha((String) lista.get("planilha"));
                record.setComentarios((String) lista.get("comentarios"));
                record.setValor((String) lista.get("valor"));
                record.setInicio((String) lista.get("inicio"));
                record.setFim((String) lista.get("fim"));
                record.setLatitude((String) lista.get("latitude"));
                record.setLongitude((String) lista.get("longitude"));
                record.setTipoFiscal((String) lista.get("tipoFiscal"));
                record.setEmailFiscal((String) lista.get("emailFiscal"));
            }

            if (params[0] == "realizado") {
                record.setIdVistoria((String) lista.get("idVistoria"));
                record.setDescricao((String) lista.get("descricao"));
                record.setValor((String) lista.get("valor"));
                record.setAvaliacao((String) lista.get("avaliacao"));
                record.setComentarios((String) lista.get("comentarios"));
            }

            if (params[0] == "foto") {
                record.setIdVistoria((String) lista.get("idVistoria"));
                record.setUri((String) lista.get("uri"));
                record.setComentarios((String) lista.get("comentarios"));
            }

            DbApi dbApi = new DbApi();
            dbApi.setBasePath(IAppConstants.DSP_URL + IAppConstants.DSP_URL_SUFIX);
            dbApi.addHeader("X-DreamFactory-Application-Name", IAppConstants.APP_NAME);
            dbApi.addHeader("X-DreamFactory-Session-Token", session_id);

            try {
                RecordResponse resultRecord = dbApi.createRecord(params[0], "1", record, null, null, null,null);
               // log(resultRecord.toString());

                return resultRecord;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(RecordResponse record) {
//            progressDialog.cancel();
            if(record != null){

                System.out.println("Create Ok!!!!!!");
            }else {

            }
        }
    }

    class UpdateRecordTask extends AsyncTask<String, Void, RecordsResponse> {

        private String errorMsg;

        @Override
        protected void onPreExecute() {
           // progressDialog.show();
        }

        @Override
        protected RecordsResponse doInBackground(String... params) {

            Map lista = getLista(params[1]);

            RecordRequest record = new RecordRequest();
            if (params[0] == "obra") {
                record.setIdObra((String) lista.get("idObra"));
                record.setDescricao((String) lista.get("descricao"));
                record.setUpdatedAt((String) lista.get("updatedAt"));
                record.setEmailFiscalCivil((String) lista.get("emailFiscalCivil"));
                record.setEmailFiscalEletrico((String) lista.get("emailFiscalEletrico"));
            }

            if (params[0] == "previsto") {
                record.setIdObra((String) lista.get("idObra"));
                record.setDescricao((String) lista.get("descricao"));
                record.setValor((String) lista.get("valor"));
            }

            if (params[0] == "vistoria") {
                record.setIdObra((String) lista.get("idObra"));
                record.setStatusObra((String) lista.get("statusObra"));
                record.setEspecificacoes((String) lista.get("especificacoes"));
                record.setDiario((String) lista.get("diario"));
                record.setProjetos((String) lista.get("projetos"));
                record.setPlanilha((String) lista.get("planilha"));
                record.setComentarios((String) lista.get("comentarios"));
                record.setValor((String) lista.get("valor"));
                record.setInicio((String) lista.get("inicio"));
                record.setFim((String) lista.get("fim"));
                record.setLatitude((String) lista.get("latitude"));
                record.setLongitude((String) lista.get("longitude"));
                record.setTipoFiscal((String) lista.get("tipoFiscal"));
                record.setEmailFiscal((String) lista.get("emailFiscal"));
                record.setDataVistoria((String) lista.get("dataVistoria"));
            }

            if (params[0] == "realizado") {
                record.setIdVistoria((String) lista.get("idVistoria"));
                record.setDescricao((String) lista.get("descricao"));
                record.setValor((String) lista.get("valor"));
                record.setAvaliacao((String) lista.get("avaliacao"));
                record.setComentarios((String) lista.get("comentarios"));
            }

            if (params[0] == "foto") {
                record.setIdVistoria((String) lista.get("idVistoria"));
                record.setUri((String) lista.get("uri"));
                record.setComentarios((String) lista.get("comentarios"));
            }

            DbApi dbApi = new DbApi();
            dbApi.setBasePath(IAppConstants.DSP_URL + IAppConstants.DSP_URL_SUFIX);
            dbApi.addHeader("X-DreamFactory-Application-Name", IAppConstants.APP_NAME);
            dbApi.addHeader("X-DreamFactory-Session-Token", session_id);

            FilterRecordRequest body = new FilterRecordRequest();
            body.setRecord(record);
            body.setFilter(params[2]); //Cláusula where

            try {
                RecordsResponse resultRecord = dbApi.replaceRecordsByFilter(params[0], body, encodeURIComponent(params[2]), null, null);
               // log(resultRecord.toString());

                return resultRecord;
            } catch (Exception e) {
                e.printStackTrace();
                errorMsg = e.getMessage();
            }
            return null;
        }
        @Override
        protected void onPostExecute(RecordsResponse record) {
          //   progressDialog.cancel();
            if(record != null){
                System.out.println("Update Ok!!!!!!!!!!!!");

            }else {
                System.out.println("Update Falhou!!!!!!!!!");
            }
        }
    }

    class DeleteRecordTask extends AsyncTask<String, Void, RecordsResponse> {

        private String errorMsg;

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected RecordsResponse doInBackground(String... params) {

            DbApi dbApi = new DbApi();
            dbApi.setBasePath(IAppConstants.DSP_URL + IAppConstants.DSP_URL_SUFIX);
            dbApi.addHeader("X-DreamFactory-Application-Name", IAppConstants.APP_NAME);
            dbApi.addHeader("X-DreamFactory-Session-Token", session_id);

            FilterRequest body = new FilterRequest();
            body.setFilter(params[1]);
            try {
                //RecordResponse resultRecord = dbApi.deleteRecord(params[0], params[1], null, null, null, null);
                RecordsResponse resultRecord = dbApi.deleteRecordsByFilter(params[0], params[1], body, null, null, null);
                log(resultRecord.toString());
                return resultRecord;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(RecordsResponse record) {
            if(record != null){
                System.out.println("Delete Ok!!!!!!!!!!!!");

            }else {
                System.out.println("Delete Falhou!!!!!!!!!");
            }
        }
    }

    public class GetRecordsTask extends AsyncTask<String, RecordsResponse, RecordsResponse>{

        @Override
        protected RecordsResponse doInBackground(String... params) {
            DbApi dbApi = new DbApi();
            dbApi.addHeader("X-DreamFactory-Application-Name", IAppConstants.APP_NAME);
            dbApi.addHeader("X-DreamFactory-Session-Token", session_id);
            dbApi.setBasePath(IAppConstants.DSP_URL + IAppConstants.DSP_URL_SUFIX);
            System.out.println("ParametrosGet" + params[1]);

            Integer filtro = -1;

            if (params.length > 2)
                filtro = Integer.parseInt(params[2]);

            try {
                RecordsResponse records = dbApi.getRecordsByFilter(params[0], encodeURIComponent(params[1]), filtro, -1, null, null, false, false, null);
                //log(records.toString());
                return records;
            } catch (Exception e) {
                e.printStackTrace();
             }
            return null;
        }

        @Override
        protected void onPostExecute(RecordsResponse records) {

            if(records != null){
                System.out.println("Get Ok!!!!!!!!!!!!");
            }else {

            }
        }
    }
    /*
    Transforma String numa lista para manipular os valores
     */
    public Map<String,String> getLista(String param) {
        String value = param;
        String[] arrValue = value.split(",");
        Map<String,String> valueMap = new HashMap<String, String>();
        for (String string : arrValue) {
            String[] mapPair = string.split("=");
            if(mapPair!=null && mapPair.length>=2)
             valueMap.put(mapPair[0].replace("[","").replace("]","").trim(), mapPair[1].replace("[","").replace("]","").trim());
        }
        return valueMap;
    }

    /*Função encodeURIComponet criada para tratar o "where" do SQL que foi passado por parametro. Pode ser melhorada.
     Falta tratar os caracteres "%" e "\";
    */
    public String encodeURIComponent(String s)
    {
        String result = null;
        try
        {
            result = s
                    .replace(" ", "%20")
                    .replace("+", "%2B")
                    .replace("/", "%2F")
                    .replace("@", "%40")
                    .replace("#", "%23")
                    .replace("$", "%24")
                    .replace("&", "%26")
                    .replace(",", "%2C")
                    .replace(":", "%3A")
                    .replace(";", "%3B")
                    .replace("=", "%3D")
                    .replace("?", "%3F")
                    .replace("\"", "%22")
                    .replace("<", "%3C")
                    .replace(">", "%3E")
                    .replace("[", "%5B")
                    .replace("]", "%5D")
                    .replace("^", "%5E")
                    .replace("{", "%7B")
                    .replace("|", "%7C")
                    .replace("}", "%7D");
            //.replace("%", "%25");
        }

        // This exception should never occur.
        catch (Exception e)
        {
            result = s;
        }
        return result;
    }
}