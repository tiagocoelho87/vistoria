
package br.gov.ba.prodeb.educacao.vistoria;

import org.androidannotations.annotations.EApplication;

import android.app.Application;

import com.parse.Parse;
import com.parse.PushService;

@EApplication
public class VistoriaApplication extends Application
{

	private long obraId = 0;
	private long vistoriaId = 0;
	
	public boolean dualPane = false;
	
	@Override
    public void onCreate() {
        super.onCreate();
        //AQUI Substituir abaixo
        Parse.initialize(this, "VcTXujnMm1BazO26wHnj03agbapGdjPqgJtLsf8w", "Zrf7J2zbPE32qalpsgT99qwDFtTZQXbzySkNkOiO"); 
        PushService.setDefaultPushCallback(this, VistoriaListActivity_.class);
        }

	public long getVistoriaId() {
		return vistoriaId;
	}

	public void setVistoriaId(long vistoriaId) {
		this.vistoriaId = vistoriaId;
	}

	public long getObraId() {
		return obraId;
	}

	public void setObraId(long obraId) {
		this.obraId = obraId;
	}

	public boolean isDualPane() {
		return dualPane;
	}

	public void setDualPane(boolean dualPane) {
		this.dualPane = dualPane;
	}

}
