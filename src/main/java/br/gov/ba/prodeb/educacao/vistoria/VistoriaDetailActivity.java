package br.gov.ba.prodeb.educacao.vistoria;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.res.StringArrayRes;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import com.google.analytics.tracking.android.EasyTracker;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

@EActivity(R.layout.vistoria_detail_activity)
@OptionsMenu(R.menu.vistoria_actions)
public class VistoriaDetailActivity extends Activity {

	@StringArrayRes
    String[] forms_vistoria;
	
	@Bean
	VistoriaAtual vistoriaAtual;
	
	@Bean
	Relatorio relatorio;
	
	@App
	VistoriaApplication vistoriaApp;
	
	@InstanceState
	int tabAtual;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			System.out.println("chegou na activity landscape");
			finish();
			return;
		}
		
		if (savedInstanceState == null) {
			System.out.println("chegou na activity saved Instance");
			VistoriaDetailFragmentServicos fragment = new VistoriaDetailFragmentServicos_();
			getFragmentManager().beginTransaction()
					.add(R.id.vistoria_detail_container, fragment).commit();
		}
	}

    @Override
    public void onPause() {
      super.onPause();
      tabAtual = getActionBar().getSelectedNavigationIndex();
    }

    @Override
    public void onResume() {
      super.onResume();
	  getActionBar().setTitle("Realizar Vistoria");
	  getActionBar().removeAllTabs();
	  getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	  
	  vistoriaAtual.activity = this;
	  vistoriaAtual.loadVistoria();
    }

    @Override
    public void onStart() {
      super.onStart();
      EasyTracker.getInstance(this).activityStart(this);  // Add this method.
    }

    @Override
    public void onStop() {
      super.onStop();
      EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }
	
    @OptionsItem(R.id.concluir)
    void concluirItem() {
    	new AlertDialog.Builder(this)
		.setIcon(R.drawable.ic_launcher)
		.setTitle("ENCERRAR")
		.setMessage("Após ser concluida uma vistoria não pode mais ser editada. Confirma?")
		.setCancelable(false)
		.setNegativeButton("Não", null)
		.setPositiveButton("Sim",
			new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
        		vistoriaAtual.salvaVistoria();
            	if (vistoriaAtual.verificaCampos()) {
            		vistoriaAtual.concluiVistoria();
            		goBack();
            	}
            }
        })
		.show();
    }
    
    @OptionsItem(R.id.salvar)
    void salvarItem() {
    	vistoriaAtual.salvaVistoria();
    	Crouton.makeText(this, "OK!", Style.CONFIRM).show();
    }

    @OptionsItem(R.id.relatorio)
    void startRelatorios() {
    	vistoriaAtual.salvaVistoria();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(relatorio.generate(), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }
    
    @Override
    public void onBackPressed() {
		goBack();
    }

	@OptionsItem(android.R.id.home)
    boolean voltar() {
		goBack();
		return true;
	}
	
	void goBack() {
		vistoriaApp.setObraId(0);
    	NavUtils.navigateUpFromSameTask(this);
	}

}
