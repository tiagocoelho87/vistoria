package br.gov.ba.prodeb.educacao.vistoria;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import br.gov.ba.prodeb.educacao.vistoria.adapter.LoaderObrasVistorias;
import br.gov.ba.prodeb.educacao.vistoria.adapter.VistoriaAdapter;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;

import br.gov.ba.prodeb.educacao.vistoria.syncadapter.SyncUtils;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.LoaderManager;
import android.app.Notification.Action;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.ListFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

@EFragment
@OptionsMenu(R.menu.vistoria_actions)
public class VistoriaListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
		
	@InstanceState
	int mCurCheckPosition = ListView.INVALID_POSITION;
	
	@InstanceState
	int tabAtual;
	
	@Bean
	VistoriaAtual vistoriaAtual;
	
	@Bean
	Relatorio relatorio;
	
	@App
	VistoriaApplication vistoriaApp;

	private VistoriaAdapter vistoriaAdapter;
	
	private static final int LOADER_ID = 1932;
	
	Location bestResult;
			
    @Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
			      new IntentFilter("RELOAD_OBRAS"));
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
			      new IntentFilter("END_SYNC"));

		View detailsFrame = getActivity().findViewById(R.id.vistoria_detail_container);
		vistoriaApp.setDualPane( detailsFrame != null
				&& detailsFrame.getVisibility() == View.VISIBLE);
		
		getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		getListView().setBackgroundColor(getResources().getColor(R.color.cinzaclaro_listview));
		getListView().setSelector(R.drawable.list_selectors);

		if (getActivity().getActionBar().getNavigationMode() == ActionBar.NAVIGATION_MODE_TABS) {
		        getActivity().getActionBar().setSelectedNavigationItem(tabAtual);
		}
		
		setHasOptionsMenu(false);
		setEmptyText("Nenhuma obra");	
		setListShown(false);
		vistoriaAdapter = new VistoriaAdapter(this.getActivity(), null, 0);
        setListAdapter(vistoriaAdapter);
		getLoaderManager().initLoader(LOADER_ID, null, this);
        
		vistoriaAtual.obraId = vistoriaApp.getObraId();
		if (vistoriaAtual.obraId != 0) {
			showDetails();
		}

	}
	
	@ItemClick
	public void listItemClicked(final int position) {
       carregaVistoria(position);
	}
	
	void carregaVistoria(int position) {
		mCurCheckPosition = position;
		vistoriaAtual.obraId = getListView().getItemIdAtPosition(position);
		vistoriaApp.setObraId(vistoriaAtual.obraId);
		//TODO Avaliar pegar logo o cursor
//		Cursor c = (Cursor) getListView().getItemAtPosition(position);
//		Toast.makeText(getActivity(), c.getString(c.getColumnIndex(ObraContract.CODIGO)), Toast.LENGTH_SHORT).show();
		getActivity().invalidateOptionsMenu();
		showDetails();			
	}
	
	void showDetails() {

		vistoriaAtual.localizacao = bestResult;
		if (vistoriaApp.isDualPane()) {
			this.setHasOptionsMenu(true);
			getActivity().getActionBar().removeAllTabs();
			getActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
			vistoriaAtual.activity = this.getActivity();
			vistoriaAtual.loadVistoria();
		} else {
			Intent intent = new Intent();
			intent.setClass(getActivity(), VistoriaDetailActivity_.class);
			startActivity(intent);
		}
	}
		
    @OptionsItem(R.id.concluir)
    void concluirItem() {
    	String title = "ENCERRAR";
    	String msg = "Após ser encerrada uma vistoria não pode mais ser editada. Confirma?";
    	if (vistoriaAtual.vistoria.getStatusObra().equals("Concluída")) {
    		title = "ENCERRAR E CONCLUIR";
    		msg = "Esta vistoria será encerrada e a obra concluída. Confirma?";
    	}
    	new AlertDialog.Builder(this.getActivity())
		.setIcon(R.drawable.ic_launcher)
		.setTitle(title)
		.setMessage(msg)
		.setCancelable(false)
		.setNegativeButton("Não", null)
		.setPositiveButton("Sim",
			new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
        		vistoriaAtual.salvaVistoria();
            	if (vistoriaAtual.verificaCampos()) {
            		vistoriaAtual.concluiVistoria();
        			vistoriaApp.setObraId(0);
        			getActivity().getActionBar().removeAllTabs();
    				getActivity().getActionBar().setSubtitle("Selecione um ítem");
        	        getLoaderManager().getLoader(LOADER_ID).forceLoad();
        			getActivity().invalidateOptionsMenu();
            	}
            }
        })
		.show();
    }
    
    @OptionsItem(R.id.salvar)
    void salvarItem() {
    	vistoriaAtual.salvaVistoria();
    	vistoriaAdapter.notifyDataSetChanged();
    	Crouton.makeText(this.getActivity(), "OK!", Style.CONFIRM).show();
    }

    @OptionsItem(R.id.relatorio)
    void relatorio() {
    	vistoriaAtual.salvaVistoria();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(relatorio.generate(), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }
    
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new LoaderObrasVistorias(this.getActivity());
	}

	@Override
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
		vistoriaAdapter.swapCursor(cursor);
        setListShown(true);
		
	}

	@Override
	public void onLoaderReset(Loader<Cursor> cursorLoader) {
//        setListShown(false);		
        vistoriaAdapter.swapCursor(null);
	}
    
    @Override
    public void onPause() {
      super.onPause();
      tabAtual = getActivity().getActionBar().getSelectedNavigationIndex();
    }
    
    @Override
    public void onResume() {
      super.onResume();
		getListView().setItemChecked(mCurCheckPosition, true);
		if (mCurCheckPosition == ListView.INVALID_POSITION) {
			getActivity().getActionBar().setSubtitle("Selecione um ítem");
		} else {
			vistoriaAtual.setSubTitle();
		}
		
		float bestAccuracy = 100;
		Date now = new Date();
		long minTime = now.getTime() - (90*60*1000);
		bestResult = null;
		long bestTime = 0;
		LocationManager locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
		List<String> matchingProviders = locationManager.getAllProviders();
		for (String provider: matchingProviders) {
		  Location location = locationManager.getLastKnownLocation(provider);
		  if (location != null) {
		    float accuracy = location.getAccuracy();
		    long time = location.getTime();
		        
		    if ((time > minTime && accuracy < bestAccuracy)) {
		      bestResult = location;
		      bestAccuracy = accuracy;
		      bestTime = time;
		    }
		    else if (time < minTime && 
		             bestAccuracy == Float.MAX_VALUE && time > bestTime){
		      bestResult = location;
		      bestTime = time;
		    }
		  }
		}
    } 
    
    @Override
    public void onStop() {
      super.onStop();
      LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Create account, if needed
        SyncUtils.CreateSyncAccount(activity);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
  	  @Override
  	  public void onReceive(Context context, Intent intent) {
  	        getLoaderManager().getLoader(LOADER_ID).forceLoad();
  	  }
    };

	@Override
	public void onPrepareOptionsMenu (Menu menu) {
		super.onPrepareOptionsMenu(menu);
		MenuItem itemS= menu.findItem(R.id.salvar);
		MenuItem itemC= menu.findItem(R.id.concluir);
		MenuItem itemR= menu.findItem(R.id.relatorio);
		if (vistoriaApp.getObraId() == 0) {
		    itemR.setVisible(false);
		    itemS.setVisible(false);
		    itemC.setVisible(false);
		} else {
		    itemR.setVisible(true);
		    itemS.setVisible(true);
		    itemC.setVisible(true);
		}
	}
}
