package br.gov.ba.prodeb.educacao.vistoria.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentMimeTypeVnd;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentUri;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultSortOrder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.VistoriaContract;

@DatabaseTable(tableName = Contract.VistoriaContract.TABLENAME)
@DefaultContentUri(authority=Contract.AUTHORITY, path=Contract.VistoriaContract.CONTENT_URI_PATH)
@DefaultContentMimeTypeVnd(name=Contract.VistoriaContract.MIMETYPE_NAME, type=Contract.VistoriaContract.MIMETYPE_TYPE)
public class Vistoria {
	
    @DatabaseField(columnName = BaseColumns._ID, generatedId = true)
    private int id;

    @DatabaseField
	private boolean concluida;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "obra_id")
    private Obra obra;

    @DatabaseField(canBeNull = false)
    @DefaultSortOrder
    private Date dataVistoria;
    
    @DatabaseField
	private String statusObra;
    
	@DatabaseField
	private float valorMedicao;
    
    @ForeignCollectionField
    private ForeignCollection<Realizado> servicos;
    
	@DatabaseField
	private String comentarios = "";

	@DatabaseField
	private boolean planilha = false;
	
	@DatabaseField
	private boolean projetos = false;
	
	@DatabaseField
	private boolean especificacoes = false;
	
	@DatabaseField
	private boolean diario = false;
	
    @DatabaseField
    private Date inicio;

    @DatabaseField
    private Date fim;

    @DatabaseField
    private double latitude = 0;

    @DatabaseField
    private double longitude = 0;

    @ForeignCollectionField
    private ForeignCollection<Foto> fotos;
    
    @DatabaseField
    private Date updatedAt;

    public Vistoria() {    	
    	dataVistoria = new Date();
    	inicio = dataVistoria;
    	statusObra = "Informe";
    }
	
    public Vistoria(Date dataVistoria, String statusObra, float valorMedicao) {
    	this.dataVistoria =  dataVistoria;
    	this.statusObra = statusObra;
    	this.valorMedicao = valorMedicao;
    }

    public int getId() {
        return id;
    }

    public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

    public final boolean getConcluida() {
		return this.concluida;
	}
	
	public void setConcluida(boolean concluida) {
		this.concluida = concluida;
	}
	
	public Obra getObra() {
		return obra;
	}
	public void setObra(Obra obra) {
		this.obra = obra;
	}
    public Date getDataVistoria() {
		return dataVistoria;
	}
	public void setDataVistoria(Date dataVistoria) {
		this.dataVistoria = dataVistoria;
	}
	public float getValorMedicao() {
		return valorMedicao;
	}

	public void setValorMedicao(float valorMedicao) {
		this.valorMedicao = valorMedicao;
	}

	public String getStatusObra() {
		if (this.statusObra.isEmpty()) {
			return "Informe";
		} else {
			return statusObra;			
		}
	}
	public void setStatusObra(String statusObra) {
		this.statusObra = statusObra;
	}	
	
    public ForeignCollection<Realizado> getRealizado() {
        return servicos;
    }
    
    public final String getComentarios() {
		return this.comentarios;
	}
	
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

    public final boolean getPlanilha() {
		return this.planilha;
	}
	
	public void setPlanilha(boolean planilha) {
		this.planilha = planilha;
	}

    public final boolean getProjetos() {
		return this.projetos;
	}
	
	public void setProjetos(boolean projetos) {
		this.projetos = projetos;
	}

    public final boolean getEspecificacoes() {
		return this.especificacoes;
	}
	
	public void setEspecificacoes(boolean especificacoes) {
		this.especificacoes = especificacoes;
	}

    public final boolean getDiario() {
		return this.diario;
	}
	
	public void setDiario(boolean diario) {
		this.diario = diario;
	}

	public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(VistoriaContract.STATUSOBRA, statusObra);
        values.put(VistoriaContract.VALORMEDICAO, valorMedicao);
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        values.put(VistoriaContract.UPDATED, dateFormat.format(new Date()));

        return values;
    }

    public Date getInicio() {
		return inicio;
	}
	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

    public Date getFim() {
		return fim;
	}
	public void setFim(Date fim) {
		this.fim = fim;
	}

    public final double getLatitude() {
		return this.latitude;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

    public final double getLongitude() {
		return this.longitude;
	}
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

    public ForeignCollection<Foto> getFoto() {
        return fotos;
    }

	public Vistoria fromCursor(Context context, Cursor c) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    	Date dataVistoria = null;
		try {
			dataVistoria = sdf.parse(c.getString(c.getColumnIndex(VistoriaContract.DATAVISTORIA)));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        String statusObra = c.getString(c.getColumnIndex(VistoriaContract.STATUSOBRA));
        float valorMedicao =  c.getFloat(c.getColumnIndex(VistoriaContract.VALORMEDICAO));
        return new Vistoria(dataVistoria, statusObra, valorMedicao);
    }
    
    

}
