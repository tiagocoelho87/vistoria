package br.gov.ba.prodeb.educacao.vistoria.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.PrevistoContract;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentMimeTypeVnd;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentUri;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultSortOrder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

@DatabaseTable(tableName = Contract.PrevistoContract.TABLENAME)
@DefaultContentUri(authority=Contract.AUTHORITY, path=Contract.PrevistoContract.CONTENT_URI_PATH)
@DefaultContentMimeTypeVnd(name=Contract.PrevistoContract.MIMETYPE_NAME, type=Contract.PrevistoContract.MIMETYPE_TYPE)

public class Previsto implements Serializable {
	
	@DatabaseField(columnName = BaseColumns._ID, generatedId = true)
	private int id;
	
	@DatabaseField(canBeNull = false)
    @DefaultSortOrder
	private String descricao;
	
	@DatabaseField(canBeNull = false)
	private float valor;
	
    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "obra_id")
    private Obra obra;
	
    @DatabaseField
    private Date updatedAt;

    public Previsto() {
    	
    }
    
    public Previsto(String descricao, float valor, Obra obra) {
        this.descricao = descricao;
        this.valor = valor;
        this.obra = obra;
    }

    public int getId() {
    	return id;
    }
    
    public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

    public final String getDescricao() {
		return this.descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public final float getValor() {
		return this.valor;
	}
	
	public void setValor(float valor) {
		this.valor = valor;
	}

	public Obra getObra() {
		return this.obra;
	}
	
	public void setObra(Obra obra) {
		this.obra = obra;
	}
	
    @Override
	public String toString() {
		return "Servico [descricao=" + descricao + ", valor=" + valor + ", obra="
				+ obra + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((obra == null) ? 0 : obra.hashCode());
		result = prime * result + Float.floatToIntBits(valor);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Previsto other = (Previsto) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (obra == null) {
			if (other.obra != null)
				return false;
		} else if (!obra.equals(other.obra))
			return false;
		if (Float.floatToIntBits(valor) != Float.floatToIntBits(other.valor))
			return false;
		return true;
	}

	public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(PrevistoContract.DESCRICAO, descricao);
        values.put(PrevistoContract.VALOR, valor);
        values.put(PrevistoContract.OBRA, obra.getId());
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        values.put(PrevistoContract.UPDATED, dateFormat.format(new Date()));
        return values;
    }

    public static Previsto fromCursor(Context context, Cursor c) {
        String descricao = c.getString(c.getColumnIndex(PrevistoContract.DESCRICAO));
        float valor = c.getFloat(c.getColumnIndex(PrevistoContract.VALOR));
        int obra_id = c.getInt(c.getColumnIndex(PrevistoContract.OBRA));
		Uri obraUri = Uri.parse(ObraContract.CONTENT_URI+"/"+String.valueOf(obra_id));
        Cursor cObra =   context.getContentResolver().query(obraUri, null, null, null, null);    
        cObra.moveToNext();
        Obra obra = Obra.fromCursor(context, cObra);
        cObra.close();
        return new Previsto(descricao, valor, obra);
    }

}
