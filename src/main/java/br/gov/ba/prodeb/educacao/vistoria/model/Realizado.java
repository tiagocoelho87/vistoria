package br.gov.ba.prodeb.educacao.vistoria.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.RealizadoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.VistoriaContract;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentMimeTypeVnd;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentUri;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultSortOrder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

@DatabaseTable(tableName = Contract.RealizadoContract.TABLENAME)
@DefaultContentUri(authority=Contract.AUTHORITY, path=Contract.RealizadoContract.CONTENT_URI_PATH)
@DefaultContentMimeTypeVnd(name=Contract.RealizadoContract.MIMETYPE_NAME, type=Contract.RealizadoContract.MIMETYPE_TYPE)

public class Realizado implements Serializable {
	
	@DatabaseField(columnName = BaseColumns._ID, generatedId = true)
	private int id;
	
	@DatabaseField(canBeNull = false)
    @DefaultSortOrder
	private String descricao;
	
	@DatabaseField(canBeNull = false)
	private float valor;
	
	private float previsto;

	private float valorAnterior;

	@DatabaseField(canBeNull = false)
	private String avaliacao;

	@DatabaseField
	private String comentarios;

	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "vistoria_id")
    private Vistoria vistoria;
	
    @DatabaseField
    private Date updatedAt;

    public Realizado() {
    	
    }
    
    public Realizado(String descricao, float valor, Vistoria vistoria, String avaliacao, String comentarios) {
        this.descricao = descricao;
        this.valor = valor;
        this.vistoria = vistoria;
        this.avaliacao = avaliacao;
        this.comentarios = comentarios;
    }

    public int getId() {
    	return id;
    }
    
    public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

    public final String getDescricao() {
		return this.descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public final float getValor() {
		return this.valor;
	}
	
	public void setValor(float valor) {
		this.valor = valor;
	}

	public final float getValorAnterior() {
		return this.valorAnterior;
	}
	
	public void setValorAnterior(float valorAnterior) {
		this.valorAnterior = valorAnterior;
	}

	public final float getPrevisto() {
		return this.previsto;
	}
	
	public void setPrevisto(float previsto) {
		this.previsto = previsto;
	}

	public Vistoria getVistoria() {
		return this.vistoria;
	}
	
	public void setVistoria(Vistoria vistoria) {
		this.vistoria = vistoria;
	}
	
    public final String getAvaliacao() {
    	if (this.avaliacao.isEmpty()) {
    		return "Avalie";
    	} else {
    		return this.avaliacao;    		
    	}
	}
	
	public void setAvaliacao(String avaliacao) {
		this.avaliacao = avaliacao;
	}
	
    public final String getComentarios() {
		return this.comentarios;
	}
	
	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	@Override
	public String toString() {
		return "Servico [descricao=" + descricao + ", valor=" + valor + ", vistoria="
				+ vistoria + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((vistoria == null) ? 0 : vistoria.hashCode());
		result = prime * result + Float.floatToIntBits(valor);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Realizado other = (Realizado) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (vistoria == null) {
			if (other.vistoria != null)
				return false;
		} else if (!vistoria.equals(other.vistoria))
			return false;
		if (Float.floatToIntBits(valor) != Float.floatToIntBits(other.valor))
			return false;
		return true;
	}

	public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(RealizadoContract.DESCRICAO, descricao);
        values.put(RealizadoContract.VALOR, valor);
        values.put(RealizadoContract.VISTORIA, vistoria.getId());
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        values.put(VistoriaContract.UPDATED, dateFormat.format(new Date()));
        return values;
    }

    public Realizado fromCursor(Context context, Cursor c) {
        String descricao = c.getString(c.getColumnIndex(RealizadoContract.DESCRICAO));
        float valor = c.getFloat(c.getColumnIndex(RealizadoContract.VALOR));
        int vistoria_id = c.getInt(c.getColumnIndex(RealizadoContract.VISTORIA));
		Uri vistoriaUri = Uri.parse(VistoriaContract.CONTENT_URI+"/"+String.valueOf(vistoria_id));
        Cursor cVistoria =   context.getContentResolver().query(vistoriaUri, null, null, null, null);    
        cVistoria.moveToNext();
//        Vistoria vistoria = Vistoria.fromCursor(context, cVistoria);
        cVistoria.close();
        return new Realizado(descricao, valor, vistoria, null, null);
    }

}
