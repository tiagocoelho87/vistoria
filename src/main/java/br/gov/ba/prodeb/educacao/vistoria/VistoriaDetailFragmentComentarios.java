package br.gov.ba.prodeb.educacao.vistoria;

import java.text.SimpleDateFormat;

import android.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import br.gov.ba.prodeb.educacao.vistoria.helper.DecimalDigitsInputFilter;
import br.gov.ba.prodeb.educacao.vistoria.helper.InputFilterMinMax;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.vistoria_detail_fragment_comentarios )
public class VistoriaDetailFragmentComentarios extends Fragment {

	@ViewById
	TextView dataVistoria;
	
	@ViewById 
	EditText valMedicao;
	
	@ViewById
    Spinner status;

	@ViewById 
	EditText comentarios;
	
	@ViewById
	CheckBox planilha;

	@ViewById
	CheckBox projetos;

	@ViewById
	CheckBox especificacoes;

	@ViewById
	CheckBox diario;

	@Bean
	VistoriaAtual vistoriaAtual;

	@AfterViews
	void configure() {
        
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getActivity(),
		        R.array.status_vistoria, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		status.setAdapter(adapter);		

	}
	
	@Override
	public void onResume () {
		super.onResume();

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		dataVistoria.setText(sdf.format( vistoriaAtual.vistoria.getDataVistoria()));
		valMedicao.setText(String.valueOf(vistoriaAtual.vistoria.getValorMedicao()));
	
		ArrayAdapter<CharSequence> adapter = (ArrayAdapter<CharSequence>) status.getAdapter();
		if (vistoriaAtual.vistoria.getStatusObra().equals("Informe")) {
			int pos=adapter.getPosition(vistoriaAtual.statusObraAnterior);
			status.setSelection(pos);
		} else {
			int pos=adapter.getPosition(vistoriaAtual.vistoria.getStatusObra());
			status.setSelection(pos);
		}

		valMedicao.setFilters(new InputFilter[]{ new InputFilterMinMax("0.00", "999999999.99"), new DecimalDigitsInputFilter(9,2)});

		valMedicao.addTextChangedListener(new TextWatcher(){
	        public void afterTextChanged(Editable s) {
				try {
		    		vistoriaAtual.vistoria.setValorMedicao(Float.parseFloat(valMedicao.getText().toString()));						
				} catch (Exception e) {
		    		vistoriaAtual.vistoria.setValorMedicao(0);
				}			    		

	        }
	        public void beforeTextChanged(CharSequence s, int start, int count, int after){}
	        public void onTextChanged(CharSequence s, int start, int before, int count){
	        }
	    }); 
	 
	status.setOnItemSelectedListener(new OnItemSelectedListener() {
	    @Override
	    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
			vistoriaAtual.vistoria.setStatusObra(status.getItemAtPosition(status.getSelectedItemPosition()).toString());
	    }

	    @Override
	    public void onNothingSelected(AdapterView<?> parentView) {
	        // your code here
	    }

	});

			comentarios.setText(vistoriaAtual.vistoria.getComentarios());
			planilha.setChecked(vistoriaAtual.vistoria.getPlanilha());
			projetos.setChecked(vistoriaAtual.vistoria.getProjetos());
			especificacoes.setChecked(vistoriaAtual.vistoria.getEspecificacoes());
			diario.setChecked(vistoriaAtual.vistoria.getDiario());
				
			comentarios.addTextChangedListener(new TextWatcher(){
		        public void afterTextChanged(Editable s) {
		    		vistoriaAtual.vistoria.setComentarios(comentarios.getText().toString());
		        }
		        public void beforeTextChanged(CharSequence s, int start, int count, int after){}
		        public void onTextChanged(CharSequence s, int start, int before, int count){
		        }
		    }); 
		 
			planilha.setOnClickListener(new OnClickListener() {				 
				  @Override
				  public void onClick(View v) {
						vistoriaAtual.vistoria.setPlanilha(planilha.isChecked());
				  }
				});
			 
			projetos.setOnClickListener(new OnClickListener() {				 
					  @Override
					  public void onClick(View v) {
							vistoriaAtual.vistoria.setProjetos(projetos.isChecked());
					  }
					});
				 
			especificacoes.setOnClickListener(new OnClickListener() {				 
					  @Override
					  public void onClick(View v) {
							vistoriaAtual.vistoria.setEspecificacoes(especificacoes.isChecked());
					  }
					});
				 
			diario.setOnClickListener(new OnClickListener() {				 
					  @Override
					  public void onClick(View v) {
							vistoriaAtual.vistoria.setDiario(diario.isChecked());
					  }
					});
			
	}

	@Override
	public void onPause () {
		super.onPause();
	}

	@Override
	public void onDetach() {
	    super.onDetach();
	}

}
