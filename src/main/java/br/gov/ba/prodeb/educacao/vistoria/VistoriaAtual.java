package br.gov.ba.prodeb.educacao.vistoria;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;

import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.FotoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.PrevistoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.RealizadoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.VistoriaContract;
import br.gov.ba.prodeb.educacao.vistoria.model.Obra;
import br.gov.ba.prodeb.educacao.vistoria.model.Vistoria;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.widget.Toast;

@EBean(scope = Scope.Singleton)
public class VistoriaAtual {
	
	@App
	VistoriaApplication vistoriaApp;
	
    String[] forms_vistoria;

	public Obra obra = new Obra();

	public long obraId;
	public Activity activity;
	
	public long vistoriaId;
	public long vistoriaAnteriorId;
	public String statusObraAnterior;
	
	public Location localizacao = null;
	
	public Vistoria vistoria = new Vistoria();
	
	public List<String> servicos = new ArrayList<String>();

	public List<Float> realizado = new ArrayList<Float>();
	
	public List<Float> previsto = new ArrayList<Float>();

	public List<Float> realizadoAnterior = new ArrayList<Float>();

	public List<String> avaliacao = new ArrayList<String>();

	public List<String> avaliacaoAnterior = new ArrayList<String>();

	public List<String> comentario = new ArrayList<String>();

	public List<String> fotos = new ArrayList<String>();

	public List<String> comentariosFotos = new ArrayList<String>();

	public void setTabs() {
		
		forms_vistoria = activity.getResources().getStringArray(R.array.forms_vistoria);
						
		activity.getActionBar().addTab(activity.getActionBar().newTab()
                .setText(forms_vistoria[0])
                .setTabListener(new VistoriasTabListener<VistoriaDetailFragmentServicos_>(
                        activity, forms_vistoria[0], VistoriaDetailFragmentServicos_.class)));
		activity.getActionBar().addTab(activity.getActionBar().newTab()
                .setText(forms_vistoria[1])
                .setTabListener(new VistoriasTabListener<VistoriaDetailFragmentComentarios_>(
                        activity, forms_vistoria[1], VistoriaDetailFragmentComentarios_.class)));
		activity.getActionBar().addTab(activity.getActionBar().newTab()
                .setText(forms_vistoria[2])
                .setTabListener(new VistoriasTabListener<VistoriaDetailFragmentFotos_>(
                        activity, forms_vistoria[2], VistoriaDetailFragmentFotos_.class))); 
		
		
	}
	
	public void concluiVistoria() {
	    ContentValues values = new ContentValues();
	    values.clear();
        values.put(VistoriaContract.CONCLUIDA, true);   
    	activity.getContentResolver().update(Uri.parse(VistoriaContract.CONTENT_URI + "/" + vistoriaId), values, null, null);		
    	if (vistoria.getStatusObra().equals("Concluída")) {
    	    values.clear();
            values.put(ObraContract.CONCLUIDA, true);   
        	activity.getContentResolver().update(Uri.parse(ObraContract.CONTENT_URI + "/" + obraId), values, null, null);
    	}
	}
	
	public void salvaVistoria() {
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdtf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		
	    ContentValues values = new ContentValues();
	    values.clear();
//	    if (vistoria.getStatusObra().equals("Informe")) {
	    	values.put(VistoriaContract.STATUSOBRA, vistoria.getStatusObra());
//	    } else {
//	    	values.put(VistoriaContract.STATUSOBRA, "");	    	
//	    }
        values.put(VistoriaContract.VALORMEDICAO, vistoria.getValorMedicao());
        values.put(VistoriaContract.OBRA, obraId);
        values.put(VistoriaContract.CONCLUIDA, false);   
        values.put(VistoriaContract.DATAVISTORIA, sdf.format( vistoria.getDataVistoria() ));
        values.put(VistoriaContract.FIM, sdtf.format( cal.getTime() ));
        values.put(VistoriaContract.COMENTARIOS, vistoria.getComentarios());   
        values.put(VistoriaContract.PLANILHA, vistoria.getPlanilha());   
        values.put(VistoriaContract.DIARIO, vistoria.getDiario());   
        values.put(VistoriaContract.ESPECIFICACOES, vistoria.getEspecificacoes());   
        values.put(VistoriaContract.PROJETOS, vistoria.getProjetos());   
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        values.put(VistoriaContract.UPDATED, dateFormat.format(new Date()));

        if (vistoriaId == 0) {
        	if (localizacao != null) {
                values.put(VistoriaContract.LATITUDE, localizacao.getLatitude());
                values.put(VistoriaContract.LONGITUDE, localizacao.getLongitude());        		
        	}
            values.put(VistoriaContract.INICIO, sdtf.format( vistoria.getInicio() ));
            Uri vistoriaUri = activity.getContentResolver().insert(VistoriaContract.CONTENT_URI, values);    
            vistoriaId = ContentUris.parseId(vistoriaUri);
            System.out.println("Vistoriaaaaaaa agora>>>>>"+vistoriaId);
        } else {
        	activity.getContentResolver().update(Uri.parse(VistoriaContract.CONTENT_URI + "/" + vistoriaId), values, null, null);
        }
        
    	for (int i=0; i < servicos.size(); i++) {
    		Cursor c = activity.getContentResolver().query(RealizadoContract.CONTENT_URI, null, RealizadoContract.VISTORIA + "=? AND " + RealizadoContract.DESCRICAO + "=?", new String[] {String.valueOf(vistoriaId), servicos.get(i)}, null);
    		boolean ok = false;
    		if (c.moveToNext()) {
    			ok = true;
    		}

			values.clear();
	        values.put(RealizadoContract.UPDATED, dateFormat.format(new Date()));
			values.put(RealizadoContract.DESCRICAO, servicos.get(i));
			values.put(RealizadoContract.VALOR, realizado.get(i));
			if (!(avaliacao.get(i).equals("Avalie"))) {
				values.put(RealizadoContract.AVALIACAO, avaliacao.get(i));				
			} else {
				values.put(RealizadoContract.AVALIACAO, "");
			}
			values.put(RealizadoContract.COMENTARIOS, comentario.get(i));
			values.put(RealizadoContract.VISTORIA, vistoriaId);
			    
    		if (ok) {
    		    activity.getContentResolver().update(Uri.parse(RealizadoContract.CONTENT_URI + "/" + c.getLong(c.getColumnIndex(RealizadoContract._ID))), values, null, null);
    		} else {
    			activity.getContentResolver().insert(RealizadoContract.CONTENT_URI, values);
    		} 
    		c.close();
    	}

    	for (int i=0; i < fotos.size(); i++) {
    		Cursor c = activity.getContentResolver().query(FotoContract.CONTENT_URI, null, FotoContract.VISTORIA + "=? AND " + FotoContract.URI + "=?", new String[] {String.valueOf(vistoriaId), fotos.get(i)}, null);
    		boolean ok = false;
    		if (c.moveToNext()) {
    			ok = true;
    		}

			values.clear();
	        values.put(FotoContract.UPDATED, dateFormat.format(new Date()));
			values.put(FotoContract.URI, fotos.get(i));
			values.put(FotoContract.COMENTARIO, comentariosFotos.get(i));
			values.put(FotoContract.VISTORIA, vistoriaId);
			    
    		if (ok) {
    		    activity.getContentResolver().update(Uri.parse(FotoContract.CONTENT_URI + "/" + c.getLong(c.getColumnIndex(FotoContract._ID))), values, null, null);
    		} else {
    			activity.getContentResolver().insert(FotoContract.CONTENT_URI, values);
    		} 
    		c.close();
    	}

    	//    	Toast.makeText(activity, "status obra " + vistoria.getStatusObra() , Toast.LENGTH_SHORT).show();

//    	Crouton.makeText(activity, "OK!", Style.CONFIRM).show();
	}

	public void setSubTitle() {
		activity.getActionBar().setSubtitle(obra.getCodigo());
	}
	
	public void loadVistoria() {
		loadVistoria(0);
	}
	

		public void loadVistoria(long searchVistoriaId) {

		vistoria = new Vistoria();
		servicos.clear();
		realizado.clear();
		previsto.clear();
		realizadoAnterior.clear();
		avaliacao.clear();
		avaliacaoAnterior.clear();
		comentario.clear();
		fotos.clear();
		comentariosFotos.clear();
		statusObraAnterior="Informe";

		Cursor c =  activity.getContentResolver().query(Uri.parse(ObraContract.CONTENT_URI + "/" + obraId), null, null, null, null);
		c.moveToNext();
		obra = Obra.fromCursor(activity, c);
		c.close();

		setSubTitle();

		c = activity.getContentResolver().query(PrevistoContract.CONTENT_URI, null, PrevistoContract.OBRA + "=?", new String[] {String.valueOf(obraId)}, PrevistoContract.DESCRICAO);
		while (c.moveToNext())
		{
			servicos.add(c.getString(c.getColumnIndex(PrevistoContract.DESCRICAO))); 
			realizado.add(0f);
			previsto.add(c.getFloat(c.getColumnIndex(PrevistoContract.VALOR)));
			realizadoAnterior.add(0f);
			avaliacao.add("Avalie");
			comentario.add("");
		}
		c.close();

		if (searchVistoriaId == 0) {
			c =  activity.getContentResolver().query(VistoriaContract.CONTENT_URI, null, VistoriaContract.OBRA + "=?", new String[] {String.valueOf(obraId)}, VistoriaContract.FIM + " DESC");
		} else {
			c =  activity.getContentResolver().query(VistoriaContract.CONTENT_URI, null, VistoriaContract._ID + "=?", new String[] {String.valueOf(searchVistoriaId)}, null);			
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if (c.moveToNext()) {
			if ((c.getInt(c.getColumnIndex(VistoriaContract.CONCLUIDA)) == 0) || (searchVistoriaId != 0)) {
				vistoriaId = c.getLong(c.getColumnIndex(VistoriaContract._ID));
				int i = 0;
				Cursor r = activity.getContentResolver().query(RealizadoContract.CONTENT_URI, null, RealizadoContract.VISTORIA + "=?", new String[] {String.valueOf(vistoriaId)}, RealizadoContract.DESCRICAO);
				while (r.moveToNext())
				{
					realizado.set(i, r.getFloat(r.getColumnIndex(RealizadoContract.VALOR)));
					avaliacao.set(i, r.getString(r.getColumnIndex(RealizadoContract.AVALIACAO)));
					comentario.set(i, r.getString(r.getColumnIndex(RealizadoContract.COMENTARIOS)));
		    		i++;
				}
				r.close();
				try {
					vistoria.setDataVistoria(sdf.parse(c.getString(c.getColumnIndex(VistoriaContract.DATAVISTORIA))));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        vistoria.setValorMedicao(c.getFloat(c.getColumnIndex(VistoriaContract.VALORMEDICAO)));
		        vistoria.setStatusObra(c.getString(c.getColumnIndex(VistoriaContract.STATUSOBRA)));
		        vistoria.setComentarios(c.getString(c.getColumnIndex(VistoriaContract.COMENTARIOS)));
		        if (c.getInt(c.getColumnIndex(VistoriaContract.PLANILHA)) == 0) {
			        vistoria.setPlanilha(false);		        	
		        } else {
			        vistoria.setPlanilha(true);
		        }
		        if (c.getInt(c.getColumnIndex(VistoriaContract.PROJETOS)) == 0) {
			        vistoria.setProjetos(false);		        	
		        } else {
			        vistoria.setProjetos(true);
		        }
		        if (c.getInt(c.getColumnIndex(VistoriaContract.ESPECIFICACOES)) == 0) {
			        vistoria.setEspecificacoes(false);		        	
		        } else {
			        vistoria.setEspecificacoes(true);
		        }
		        if (c.getInt(c.getColumnIndex(VistoriaContract.DIARIO)) == 0) {
			        vistoria.setDiario(false);		        	
		        } else {
			        vistoria.setDiario(true);
		        }
		        
		        c.close();
		        
				c = activity.getContentResolver().query(FotoContract.CONTENT_URI, null, FotoContract.VISTORIA + "=?", new String[] {String.valueOf(vistoriaId)}, null);
				while (c.moveToNext())
				{
					fotos.add(c.getString(c.getColumnIndex(FotoContract.URI))); 
					comentariosFotos.add(c.getString(c.getColumnIndex(FotoContract.COMENTARIO)));
				}
				c.close();				
		        
			} else {
				vistoriaId = 0;
			}
		} else {
			vistoriaId = 0;
		}
		

        //Vistoria anterior
		c =  activity.getContentResolver().query(VistoriaContract.CONTENT_URI, null, VistoriaContract.OBRA + "=? AND " + VistoriaContract.CONCLUIDA + "=1", new String[] {String.valueOf(obraId)}, VistoriaContract.FIM + " DESC");
		if (c.moveToNext()) {
			vistoriaAnteriorId = c.getLong(c.getColumnIndex(VistoriaContract._ID));
			statusObraAnterior = c.getString(c.getColumnIndex(VistoriaContract.STATUSOBRA));
			int i = 0;
			Cursor r = activity.getContentResolver().query(RealizadoContract.CONTENT_URI, null, RealizadoContract.VISTORIA + "=?", new String[] {String.valueOf(vistoriaAnteriorId)}, RealizadoContract.DESCRICAO);
			while (r.moveToNext())
			{
				realizadoAnterior.set(i, r.getFloat(r.getColumnIndex(RealizadoContract.VALOR)));
//				avaliacaoAnterior.set(i, r.getString(r.getColumnIndex(RealizadoContract.AVALIACAO)));
				i++;
			}
			r.close();		        	
		}

		c.close();

		if (vistoriaId == 0) {
		}
        c.close();
		
        
		if (searchVistoriaId == 0) {
			setTabs();
		}

	}

	public boolean verificaCampos() {
		if (vistoria.getStatusObra().equals("Informe")) {
			Crouton.makeText(activity, "Status da obra é obrigatório!", Style.ALERT).show();
			return false;
		}
		for (int i=0; i < servicos.size(); i++) {
			if (realizado.get(i) > 0) {
				if ((avaliacao.get(i).equals("Avalie")) || (comentario.get(i).isEmpty())) {
					Crouton.makeText(activity, "É necessário avaliar e comentar todos os serviços que tenha sido executados!", Style.ALERT).show();
					return false;					
				}
			}
			if (avaliacao.get(i).equals("Não avaliado")) {
				if (comentario.get(i).isEmpty()) {
					Crouton.makeText(activity, "É necessário comentar todos os serviços não avaliados!", Style.ALERT).show();
					return false;					
				}
			}
		}
		
		if (fotos.size() == 0) {
			Crouton.makeText(activity, "É necessário cadastrar fotos!", Style.ALERT).show();
			return false;								
		}
		
		for (int i=0; i < comentariosFotos.size(); i++) {
			if (comentariosFotos.get(i).isEmpty()) {
					Crouton.makeText(activity, "É necessário comentar todas as fotos!", Style.ALERT).show();
					return false;					
				}
		}
		return true;
	}

}
