package br.gov.ba.prodeb.educacao.vistoria.contentprovider;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import br.gov.ba.prodeb.educacao.vistoria.model.Foto;
import br.gov.ba.prodeb.educacao.vistoria.model.Obra;
import br.gov.ba.prodeb.educacao.vistoria.model.Previsto;
import br.gov.ba.prodeb.educacao.vistoria.model.Realizado;
import br.gov.ba.prodeb.educacao.vistoria.model.Vistoria;

public class Helper extends OrmLiteSqliteOpenHelper {
    public Helper(Context context) {
        super(context, "Vistorias", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, Obra.class);
            TableUtils.createTableIfNotExists(connectionSource, Previsto.class);
            TableUtils.createTableIfNotExists(connectionSource, Realizado.class);
            TableUtils.createTableIfNotExists(connectionSource, Vistoria.class);
            TableUtils.createTableIfNotExists(connectionSource, Foto.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Obra.class, true);
            TableUtils.createTable(connectionSource, Obra.class);
            TableUtils.dropTable(connectionSource, Previsto.class, true);
            TableUtils.createTable(connectionSource, Previsto.class);
            TableUtils.dropTable(connectionSource, Realizado.class, true);
            TableUtils.createTable(connectionSource, Realizado.class);
            TableUtils.dropTable(connectionSource, Vistoria.class, true);
            TableUtils.createTable(connectionSource, Vistoria.class);
            TableUtils.dropTable(connectionSource, Foto.class, true);
            TableUtils.createTable(connectionSource, Foto.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
