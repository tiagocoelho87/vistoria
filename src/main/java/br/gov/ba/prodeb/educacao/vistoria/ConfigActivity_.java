//
// DO NOT EDIT THIS FILE, IT HAS BEEN GENERATED USING AndroidAnnotations 3.0.1.
//


package br.gov.ba.prodeb.educacao.vistoria;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.df.adapters.ToDoAdapter;

import br.gov.ba.prodeb.educacao.vistoria.R.id;
import br.gov.ba.prodeb.educacao.vistoria.R.layout;
import org.androidannotations.api.view.HasViews;
import org.androidannotations.api.view.OnViewChangedListener;
import org.androidannotations.api.view.OnViewChangedNotifier;

import java.util.List;

public final class ConfigActivity_
    extends ConfigActivity
    implements HasViews, OnViewChangedListener
{

    private final OnViewChangedNotifier onViewChangedNotifier_ = new OnViewChangedNotifier();
    private ListView list_view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        OnViewChangedNotifier previousNotifier = OnViewChangedNotifier.replaceNotifier(onViewChangedNotifier_);
        init_(savedInstanceState);
        super.onCreate(savedInstanceState);
        OnViewChangedNotifier.replaceNotifier(previousNotifier);
        setContentView(layout.config_activity);

        //    Tentativa frustrada
        /*Intent intent = getIntent();
        List records = (List)intent.getStringArrayListExtra("records");
        if (records != null)
            list_view.setAdapter(new ToDoAdapter(ConfigActivity_.this, records));
            */
    }

    private void init_(Bundle savedInstanceState) {
        OnViewChangedNotifier.registerOnViewChangedListener(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        onViewChangedNotifier_.notifyViewChanged(this);
    }

    @Override
    public void setContentView(View view, LayoutParams params) {
        super.setContentView(view, params);
        onViewChangedNotifier_.notifyViewChanged(this);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        onViewChangedNotifier_.notifyViewChanged(this);
    }

    public static ConfigActivity_.IntentBuilder_ intent(Context context) {
        return new ConfigActivity_.IntentBuilder_(context);
    }

    public static ConfigActivity_.IntentBuilder_ intent(android.app.Fragment fragment) {
        return new ConfigActivity_.IntentBuilder_(fragment);
    }

    public static ConfigActivity_.IntentBuilder_ intent(android.support.v4.app.Fragment supportFragment) {
        return new ConfigActivity_.IntentBuilder_(supportFragment);
    }

    @Override
    public void onViewChanged(HasViews hasViews) {
        sync_auto = ((CheckBox) hasViews.findViewById(id.sync_auto));
        ultima = ((TextView) hasViews.findViewById(id.ultima));
        eletrico = ((RadioButton) hasViews.findViewById(id.eletrico));
        email = ((EditText) hasViews.findViewById(id.email));
        //texto = ((EditText) hasViews.findViewById(id.texto));
        civil = ((RadioButton) hasViews.findViewById(id.civil));
        {
            View view = hasViews.findViewById(id.resetSync);
            if (view!= null) {
                view.setOnClickListener(new OnClickListener() {


                    @Override
                    public void onClick(View view) {
                        ConfigActivity_.this.resetSync();
                    }

                }
                );
            }
        }

    /*    { View view = hasViews.findViewById(id.btn2);
            if (view!= null) {
                view.setOnClickListener(new OnClickListener() {

                                            @Override
                                            public void onClick(View view) {
                                                ConfigActivity_.this.testnow2();
                                            }

                                        }
                );
            }
        }
        {
            View view = hasViews.findViewById(id.btn1);
            if (view!= null) {
                view.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ConfigActivity_.this.testnow3();
                    }
                });
            }
        }*/
        {
            View view = hasViews.findViewById(id.btnSyncNow);
            if (view!= null) {
                view.setOnClickListener(new OnClickListener() {


                    @Override
                    public void onClick(View view) {
                        ConfigActivity_.this.syncNow();
                    }

                }
                );
            }
        }
        {
            CompoundButton view = ((CompoundButton) hasViews.findViewById(id.sync_auto));
            if (view!= null) {
                view.setOnCheckedChangeListener(new OnCheckedChangeListener() {


                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        ConfigActivity_.this.checkedChangedSyncCheckBox();
                    }

                }
                );
            }
        }
    }

    public static class IntentBuilder_ {

        private Context context_;
        private final Intent intent_;
        private android.app.Fragment fragment_;
        private android.support.v4.app.Fragment fragmentSupport_;

        public IntentBuilder_(Context context) {
            context_ = context;
            intent_ = new Intent(context, ConfigActivity_.class);
        }

        public IntentBuilder_(android.app.Fragment fragment) {
            fragment_ = fragment;
            context_ = fragment.getActivity();
            intent_ = new Intent(context_, ConfigActivity_.class);
        }

        public IntentBuilder_(android.support.v4.app.Fragment fragment) {
            fragmentSupport_ = fragment;
            context_ = fragment.getActivity();
            intent_ = new Intent(context_, ConfigActivity_.class);
        }

        public Intent get() {
            return intent_;
        }

        public ConfigActivity_.IntentBuilder_ flags(int flags) {
            intent_.setFlags(flags);
            return this;
        }

        public void start() {
            context_.startActivity(intent_);
        }

        public void startForResult(int requestCode) {
            if (fragmentSupport_!= null) {
                fragmentSupport_.startActivityForResult(intent_, requestCode);
            } else {
                if (fragment_!= null) {
                    fragment_.startActivityForResult(intent_, requestCode);
                } else {
                    if (context_ instanceof Activity) {
                        ((Activity) context_).startActivityForResult(intent_, requestCode);
                    } else {
                        context_.startActivity(intent_);
                    }
                }
            }
        }

    }

}
