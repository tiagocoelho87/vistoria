package br.gov.ba.prodeb.educacao.vistoria;

import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.ListView;
import br.gov.ba.prodeb.educacao.vistoria.adapter.LoaderObras;
import br.gov.ba.prodeb.educacao.vistoria.adapter.ObraAdapter;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ItemClick;

@EFragment
public class ObraListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
	
	@InstanceState
	int mCurCheckPosition = ListView.INVALID_POSITION;
		
	@App
	VistoriaApplication vistoriaApp;
	
	private static final int LOADER_ID = 1933;
	
	private ObraAdapter obraAdapter;

    @Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
			      new IntentFilter("RELOAD_OBRAS"));
		
		getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		getListView().setBackgroundColor(getResources().getColor(R.color.cinzaclaro_listview));
		getListView().setSelector(R.drawable.list_selectors);

		setEmptyText("Nenhuma obra");	
		setListShown(false);
		obraAdapter = new ObraAdapter(this.getActivity(), null, 0);
        setListAdapter(obraAdapter);
		getLoaderManager().initLoader(LOADER_ID, null, this);
        
//		if (vistoriaApp.getObraId() != 0) {
//			showDetails();					
//		}

	}
	
	@ItemClick
	public void listItemClicked(int position) {
		mCurCheckPosition = position;
		vistoriaApp.setObraId(this.getListView().getItemIdAtPosition(position));
		showDetails();		
	}
	
	void showDetails() {
		if (vistoriaApp.isDualPane()) {
			
			ObraDetailFragment details = (ObraDetailFragment_) getFragmentManager()
					.findFragmentById(R.id.obra_detail_container);
			if (details == null) {

				details = ObraDetailFragment_.builder()
						.build();

				FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				ft.replace(R.id.obra_detail_container, details);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commit();
				
			} else {
				if (details.isDetached()) {
					FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
					ft.attach(details);
					ft.commit();
				}
				details.configure();
			}
			
		} else {
							
			Intent intent = new Intent();
			
			intent.setClass(getActivity(), ObraDetailActivity_.class);
			
			startActivity(intent);
			
		}

	}

    @Override
    public void onPause() {
      super.onPause();
    }

    @Override
    public void onResume() {
      super.onResume();
		getListView().setItemChecked(mCurCheckPosition, true);
    }

    @Override
    public void onStop() {
      super.onStop();
      LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
    }
    
    @Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new LoaderObras(this.getActivity());
	}

	@Override
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
		obraAdapter.swapCursor(cursor);
        setListShown(true);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> cursorLoader) {
        obraAdapter.swapCursor(null);
	}
    
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
    	  @Override
    	  public void onReceive(Context context, Intent intent) {
    	        getLoaderManager().getLoader(LOADER_ID).forceLoad();
    	  }
    };

}
