package br.gov.ba.prodeb.educacao.vistoria;

import java.io.File;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;

import com.google.analytics.tracking.android.EasyTracker;

import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.FotoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.RealizadoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.VistoriaContract;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

@EActivity(R.layout.relatorio_list_activity)
@OptionsMenu(R.menu.relatorio_actions)
public class RelatoriosListActivity extends Activity implements OnRelatorioInteractionListener {
	

	@App
	VistoriaApplication vistoriaApp;
	
	@Bean
	VistoriaAtual vistoriaAtual;
	
	@Bean
	Relatorio relatorio;
		
	@AfterViews
	void config() {
		vistoriaAtual.activity = this;
	}
	
	
    @OptionsItem(R.id.visualizar)
	public void onShow() {
		vistoriaAtual.obraId = vistoriaApp.getObraId();
		vistoriaAtual.loadVistoria(vistoriaApp.getVistoriaId());
		if (vistoriaAtual.obra.getUee()==null) {
	    	new AlertDialog.Builder(this)
			.setIcon(R.drawable.ic_launcher)
			.setTitle("ERRO")
			.setMessage("Para emitir um relatório é necessário primeiro sincronizar os dados da obra.")
			.setCancelable(true)
			.setPositiveButton("OK",null)
        	.show();			
		} else {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(relatorio.generate(), "application/pdf");
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			startActivity(intent);
		}
	}
    
    @OptionsItem(R.id.enviar)
	public void onSend() {
		vistoriaAtual.obraId = vistoriaApp.getObraId();
		vistoriaAtual.loadVistoria(vistoriaApp.getVistoriaId());
		if (vistoriaAtual.obra.getUee()==null) {
	    	new AlertDialog.Builder(this)
			.setIcon(R.drawable.ic_launcher)
			.setTitle("ERRO")
			.setMessage("Para emitir um relatório é necessário primeiro sincronizar os dados da obra.")
			.setCancelable(true)
			.setPositiveButton("OK",null)
        	.show();			
		} else {
			Intent intent = new Intent(Intent.ACTION_SEND);
        	intent.setType("text/plain");
        	intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"email@example.com"});
        	intent.putExtra(Intent.EXTRA_SUBJECT, "Relatório de Vistoria");
        	intent.putExtra(Intent.EXTRA_TEXT, "Segue em anexo");
        	Uri uri = relatorio.generate();
        	intent.putExtra(Intent.EXTRA_STREAM, uri);
        	startActivity(Intent.createChooser(intent, "Send email..."));
		}
	}

    @OptionsItem(R.id.remover)
	public void onDelete() {
    	new AlertDialog.Builder(this)
		.setIcon(R.drawable.ic_launcher)
		.setTitle("REMOVER")
		.setMessage("Após remover uma vistoria não será possível emitir um relatório com seus dados. Confirma?")
		.setCancelable(false)
		.setNegativeButton("Não", null)
		.setPositiveButton("Sim",
			new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int id) {
        		getContentResolver().delete(Uri.parse(VistoriaContract.CONTENT_URI + "/" +vistoriaApp.getVistoriaId()), null, null);
        		getContentResolver().delete(RealizadoContract.CONTENT_URI, RealizadoContract.VISTORIA + "=?", new String[]{String.valueOf(vistoriaApp.getVistoriaId())});
        		Cursor c = getContentResolver().query(FotoContract.CONTENT_URI, new String[]{FotoContract._ID, FotoContract.URI}, FotoContract.VISTORIA + "=?", new String[]{String.valueOf(vistoriaApp.getVistoriaId())}, null);
        		while (c.moveToNext()) {
                	File file = new File(c.getString(c.getColumnIndex(FotoContract.URI)));
                	file.delete();
        		    Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_MOUNTED");
        			File f = new File(c.getString(c.getColumnIndex(FotoContract.URI)));
        		    Uri contentUri = Uri.fromFile(f);
        		    mediaScanIntent.setData(contentUri);
        		    sendBroadcast(mediaScanIntent);
        			getContentResolver().delete(Uri.parse(FotoContract.CONTENT_URI + "/" + c.getLong(c.getColumnIndex(FotoContract._ID))), null, null);
        		}
        		c = getContentResolver().query(VistoriaContract.CONTENT_URI, new String[]{VistoriaContract._ID}, VistoriaContract.OBRA + "=?", new String[]{String.valueOf(vistoriaApp.getObraId())}, null);
        		if (c.getCount() == 0) {
            		getContentResolver().delete(Uri.parse(ObraContract.CONTENT_URI + "/" +vistoriaApp.getObraId()), null, null);        			
        		}
        		c.close();
        		 vistoriaApp.setObraId(0);
         		 vistoriaApp.setVistoriaId(0);
        		getActionBar().setSubtitle("Selecione um ítem");
        		invalidateOptionsMenu();
            }
        })
		.show();
	}
    
    @Override
    public void onPause() {
      super.onPause();
      vistoriaApp.setObraId(0);
      vistoriaApp.setVistoriaId(0);
    }
    
    
    @Override
    public void onResume() {
      super.onResume();
		getActionBar().setTitle("Relat�rios");
		getActionBar().setSubtitle("Selecione um item");
    }
    
    @Override
    public void onStart() {
      super.onStart();
      EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
      super.onStop();
      EasyTracker.getInstance(this).activityStop(this);
    }

	@Override
	public void onItemSelected(String subTitle) {
		getActionBar().setSubtitle(subTitle);
		invalidateOptionsMenu();
	}
	
	@Override
	public boolean onPrepareOptionsMenu (Menu menu) {
		super.onPrepareOptionsMenu(menu);
		if (vistoriaApp.getObraId() == 0) {
			return false;
		} else {
			return true;
		}
	}
}
