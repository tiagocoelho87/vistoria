package br.gov.ba.prodeb.educacao.vistoria.contentprovider;

import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.FotoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.PrevistoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.RealizadoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.VistoriaContract;
import br.gov.ba.prodeb.educacao.vistoria.model.Foto;
import br.gov.ba.prodeb.educacao.vistoria.model.Obra;
import br.gov.ba.prodeb.educacao.vistoria.model.Previsto;
import br.gov.ba.prodeb.educacao.vistoria.model.Realizado;
import br.gov.ba.prodeb.educacao.vistoria.model.Vistoria;

import com.tojc.ormlite.android.OrmLiteSimpleContentProvider;
import com.tojc.ormlite.android.framework.MatcherController;
import com.tojc.ormlite.android.framework.MimeTypeVnd.SubType;

public class VistoriaProvider extends OrmLiteSimpleContentProvider<Helper> {
    @Override
    protected Class<Helper> getHelperClass() {
        return Helper.class;
    }

    @Override
    public boolean onCreate() {
        setMatcherController(new MatcherController()//
                .add(Obra.class, SubType.DIRECTORY, "", ObraContract.CONTENT_URI_PATTERN_MANY)//
                .add(Obra.class, SubType.ITEM, "#", ObraContract.CONTENT_URI_PATTERN_ONE)
        		.add(Previsto.class, SubType.DIRECTORY, "", PrevistoContract.CONTENT_URI_PATTERN_MANY)//
        		.add(Previsto.class, SubType.ITEM, "#", PrevistoContract.CONTENT_URI_PATTERN_ONE)
        		.add(Realizado.class, SubType.DIRECTORY, "", RealizadoContract.CONTENT_URI_PATTERN_MANY)//
        		.add(Realizado.class, SubType.ITEM, "#", RealizadoContract.CONTENT_URI_PATTERN_ONE)
        		.add(Vistoria.class, SubType.DIRECTORY, "", VistoriaContract.CONTENT_URI_PATTERN_MANY)//
        		.add(Vistoria.class, SubType.ITEM, "#", VistoriaContract.CONTENT_URI_PATTERN_ONE)
		.add(Foto.class, SubType.DIRECTORY, "", FotoContract.CONTENT_URI_PATTERN_MANY)//
		.add(Foto.class, SubType.ITEM, "#", FotoContract.CONTENT_URI_PATTERN_ONE));
        return true;
    }

}
