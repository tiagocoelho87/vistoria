package br.gov.ba.prodeb.educacao.vistoria;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.EditText;
import android.widget.GridView;
import br.gov.ba.prodeb.educacao.vistoria.adapter.PrevistoAdapter;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.PrevistoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.VistoriaContract;
import br.gov.ba.prodeb.educacao.vistoria.model.Obra;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

@EFragment(R.layout.obra_detail_fragment)
@OptionsMenu(R.menu.obra_actions)
public class ObraDetailFragment extends Fragment {

    ToDoDemoActivity todo = new ToDoDemoActivity();
	
	@ViewById
	EditText codObra;
	
	@ViewById
	EditText descObra;
	
	@ViewById
	GridView listServicos;
	
	@Bean
	PrevistoAdapter previstoAdapter;
	
	@App
	VistoriaApplication vistoriaApp;
	
    @OptionsItem(R.id.remover)
    void removerItem() {
    	if (vistoriaApp.getObraId() == 0) {
        	new AlertDialog.Builder(this.getActivity())
    		.setIcon(R.drawable.ic_launcher)
    		.setTitle("REMOVER")
    		.setMessage("Essa obra ainda não foi salva! IdObra:"+vistoriaApp.getObraId())
    		.setCancelable(false)
    		.setNeutralButton("OK", null)
    		.show();
    	} else {
    	new AlertDialog.Builder(this.getActivity())
    		.setIcon(R.drawable.ic_launcher)
    		.setTitle("REMOVER")
    		.setMessage("Confirma?")
    		.setCancelable(false)
    		.setNegativeButton("Não", null)
    		.setPositiveButton("Sim",
    			new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int id) {
                	remover();
                }
            })
    		.show();
    	}
    }

    void remover() {
		Cursor c = this.getActivity().getContentResolver().query(VistoriaContract.CONTENT_URI, new String[]{VistoriaContract._ID}, VistoriaContract.OBRA + "=?", new String[] {String.valueOf(vistoriaApp.getObraId())}, null);
		if (c.getCount() == 0) {
            //Delete
            // todo.deleteDados("obra","idObra=" +String.valueOf(vistoriaApp.getObraId()));
			 this.getActivity().getContentResolver().delete(Uri.parse(ObraContract.CONTENT_URI + "/" + vistoriaApp.getObraId()), null, null);
			 Intent intent = new Intent("RELOAD_OBRAS");
			 LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
			vistoriaApp.setObraId(0);
			if (vistoriaApp.isDualPane()) {
				this.getFragmentManager().beginTransaction().detach(this).commit();
				this.getActivity().getActionBar().setSubtitle("Selecione um ítem");
			} else {
				this.getActivity().finish();
			}
		} else {
        	new AlertDialog.Builder(this.getActivity())
    		.setIcon(R.drawable.ic_launcher)
    		.setTitle("REMOVER")
    		.setMessage("Essa obra possui vistorias e não pode ser removida!")
    		.setCancelable(false)
    		.setNeutralButton("OK", null)
    		.show();    					
		}

    }
    
    @OptionsItem(R.id.salvar)
    void salvarItem() {
    	salvar();
    }

    private boolean umServico() {
    	for (int i=0; i < listServicos.getCount(); i++) {
    		if (((PrevistoAdapter)listServicos.getAdapter()).valores[i] > 0) {
    			return true;
    		}    		
    	}
    	return false;
    }
    
    private void salvar() {
    	if (!(codObra.getText().toString().isEmpty()) && !(descObra.getText().toString().isEmpty())) {
          if (umServico()) {
    		Obra obra = new Obra(codObra.getText().toString(),descObra.getText().toString());          
        	if (vistoriaApp.getObraId() == 0) {
        		Cursor c = this.getActivity().getContentResolver().query(ObraContract.CONTENT_URI, new String[] {ObraContract._ID}, ObraContract.CODIGO + "=?", new String[] {obra.getCodigo()}, null);
        		if (!c.moveToNext()) {
        			ContentValues values = obra.getContentValues();
        			values.put(ObraContract.CONCLUIDA, 0);
        			Uri obraUri = this.getActivity().getContentResolver().insert(ObraContract.CONTENT_URI, values);
        			vistoriaApp.setObraId(ContentUris.parseId(obraUri));
        			this.getActivity().getActionBar().setSubtitle(codObra.getText().toString());
        			
        			Crouton.makeText(this.getActivity(), "OK!", Style.CONFIRM).show();

        		} else {
        			Crouton.makeText(this.getActivity(), "Já existe uma obra com esse código!", Style.ALERT).show();
        		}
        		c.close();
        	} else {
        		Cursor c = this.getActivity().getContentResolver().query(ObraContract.CONTENT_URI, new String[] {ObraContract._ID}, ObraContract.CODIGO + "=?", new String[] {obra.getCodigo()}, null);
        		c.moveToNext();
        		if (vistoriaApp.getObraId() == c.getLong(c.getColumnIndex(ObraContract._ID))) {
        			this.getActivity().getContentResolver().update(Uri.parse(ObraContract.CONTENT_URI + "/" + vistoriaApp.getObraId()), obra.getContentValues(), null, null);
        			Crouton.makeText(this.getActivity(), "OK!", Style.CONFIRM).show();
        		} else {
        			Crouton.makeText(this.getActivity(), "Já existe uma obra com esse código!", Style.ALERT).show();
        		}
        	}
        	
        	for (int i=0; i < listServicos.getCount(); i++) {
        		Cursor c = this.getActivity().getContentResolver().query(PrevistoContract.CONTENT_URI, null, PrevistoContract.OBRA + "=? AND " + PrevistoContract.DESCRICAO + "=?", new String[] {String.valueOf(vistoriaApp.getObraId()), ((PrevistoAdapter)listServicos.getAdapter()).servicos[i]}, null);
//        		Previsto previsto = null;
        		boolean ok = false;
        		if (c.moveToNext()) {
//        			previsto = Previsto.fromCursor(getActivity(), c);
        			ok = true;
        		}

        		if (((PrevistoAdapter)listServicos.getAdapter()).valores[i] > 0) {
//            		Cursor cObra = this.getActivity().getContentResolver().query(Uri.parse(ObraContract.CONTENT_URI + "/" + curId), null, null, null, null);
//            		cObra.moveToNext();
//            		obra = Obra.fromCursor(getActivity(), cObra);
//            		cObra.close();
            		
//    				Previsto novoPrevisto = new Previsto(
//    						((PrevistoAdapter)listServicos.getAdapter()).servicos[i], 
//    						((PrevistoAdapter)listServicos.getAdapter()).valores[i],
//    						obra);
    				
        			// TODO
        			// REVER .fromCursor e .getContentValues()
        			
    			    ContentValues values = new ContentValues();
    			    values.clear();
    			    values.put(PrevistoContract.DESCRICAO, ((PrevistoAdapter)listServicos.getAdapter()).servicos[i]);
    			    values.put(PrevistoContract.VALOR, ((PrevistoAdapter)listServicos.getAdapter()).valores[i]);
    			    values.put(PrevistoContract.OBRA, vistoriaApp.getObraId());
    		        SimpleDateFormat dateFormat = new SimpleDateFormat(
    		                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    		        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    		        values.put(PrevistoContract.UPDATED, dateFormat.format(new Date()));
    			    
        			if (ok) {
        		    	this.getActivity().getContentResolver().update(Uri.parse(PrevistoContract.CONTENT_URI + "/" + c.getLong(c.getColumnIndex(PrevistoContract._ID))), values, null, null);
        			} else {
        				this.getActivity().getContentResolver().insert(PrevistoContract.CONTENT_URI, values);
        			}
        		} else {
        			if (ok) {
        		    	this.getActivity().getContentResolver().delete(Uri.parse(PrevistoContract.CONTENT_URI + "/" + c.getLong(c.getColumnIndex(PrevistoContract._ID))), null, null);
        			}
        		}
        		c.close();
        	}
			 Intent intent = new Intent("RELOAD_OBRAS");
			 LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
          } else {
      		Crouton.makeText(this.getActivity(), "Cadastre ao menos um serviço!", Style.ALERT).show();
          }
    	} else {
      		Crouton.makeText(this.getActivity(), "Código da Obra é obrigatório!", Style.ALERT).show();
        }
    }
        
	@OptionsItem(android.R.id.home)
    boolean voltar() {
		if (vistoriaApp.getObraId() == 0) {
	    	new AlertDialog.Builder(this.getActivity())
			.setIcon(R.drawable.ic_launcher)
			.setTitle("FECHAR")
			.setMessage("Obra não foi salva. Confirma?")
			.setCancelable(false)
			.setNegativeButton("Não", null)
			.setPositiveButton("Sim",
				new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog,int id) {
	            	goBack();
	            }
	        })
			.show();			
		} else {
			goBack();
		}

		return true;
	}
	
	void goBack() {
		vistoriaApp.setObraId(0);
    	NavUtils.navigateUpFromSameTask(this.getActivity());
	}

    @Override
    public void onResume() {
      super.onResume();
      configure();
    }

    public void configure() {
  		if (vistoriaApp.getObraId()==0) {
  			getActivity().getActionBar().setSubtitle("Nova obra");				
  			previstoAdapter.initAdapter();
  			codObra.setText("");
  			descObra.setText("");
  			codObra.requestFocus();
  		} else {
  			Cursor c =  this.getActivity().getContentResolver().query(Uri.parse(ObraContract.CONTENT_URI + "/" + vistoriaApp.getObraId()), null, null, null, null);
  			c.moveToNext();
            Obra obra = Obra.fromCursor(getActivity(), c);
  			c.close();
  			getActivity().getActionBar().setSubtitle(obra.getCodigo());
  			codObra.setText(obra.getCodigo());
  			descObra.setText(obra.getDescricao());
  			c = this.getActivity().getContentResolver().query(PrevistoContract.CONTENT_URI, null, PrevistoContract.OBRA + "=?", new String[] {String.valueOf(vistoriaApp.getObraId())}, PrevistoContract.DESCRICAO);
  			previstoAdapter.initAdapter(c);
  			c.close();
  		}
  		
  		listServicos.setAdapter(previstoAdapter);
      }

}
