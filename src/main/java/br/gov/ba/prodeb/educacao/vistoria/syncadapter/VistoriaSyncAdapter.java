package br.gov.ba.prodeb.educacao.vistoria.syncadapter;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.FotoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.PrevistoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.RealizadoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.VistoriaContract;

import br.gov.ba.prodeb.educacao.vistoria.ToDoDemoActivity;

public class VistoriaSyncAdapter extends AbstractThreadedSyncAdapter {

    private static final String TAG = "VistoriaSyncAdapter";

    private Context mContext;

    ToDoDemoActivity todo = new ToDoDemoActivity();

    public VistoriaSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContext = context;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public VistoriaSyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContext = context;
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              final ContentProviderClient provider, SyncResult syncResult) {


        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
        String emailFiscal = settings.getString("Email", "");
        String tipoFiscal = settings.getString("tipoFiscal", "civil");
        String ultimaAtualizacao = settings.getString("ultimaAtualizacao", "2001-01-01 00:00:00");
        //String ultimaAtualizacao = "2001-01-01 00:00:00";

        if (!(emailFiscal.isEmpty())) {

            StringBuilder whereSQL = new StringBuilder();
            StringBuilder whereSQLdata = new StringBuilder();
            String whereSQLdata2 = new String();
            whereSQLdata.append("updatedat >\"" + ultimaAtualizacao + "\"");

            if (tipoFiscal.equals("civil")) {
                whereSQL.append(" and ").append("emailFiscalCivil =\"" + emailFiscal + "\"");
            } else {
                whereSQL.append(" and ").append("emailFiscalEletrico =\"" + emailFiscal + "\"");
            }

            AsyncTask records = todo.getDados2("obra", whereSQLdata.toString() + whereSQL.toString());

            try {
                System.out.println("Records do getDados2="+ records.get().toString());
                //Enquanto o resultado não for vazio num while pegue o proximo
                while (!records.get().toString().equals("[]")){

                String value = records.get().toString();
                String[] arrValue = value.split(",");
                Map<String,String> listaMap = new HashMap<String, String>();
                for (String string : arrValue) {
                    String[] mapPair = string.split("=");
                    if(mapPair!=null && mapPair.length>=2)
                    listaMap.put(mapPair[0].replace("[","").replace("]","").trim(), mapPair[1].replace("[","").replace("]","").trim());
                }

                if (!records.get().toString().equals("[]"))
                mergeLocal(provider, listaMap, ultimaAtualizacao);  //Atualiza os dados do DreamFactory no SQLite interno

                whereSQLdata2 = "updatedat >\"" + String.valueOf(listaMap.get("updatedAt")) + "\"";
                records = todo.getDados2("obra", whereSQLdata2 + whereSQL.toString());
                }
            } catch (Exception e) { e.printStackTrace(); }

            SharedPreferences settings2 = PreferenceManager.getDefaultSharedPreferences(getContext());
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            SharedPreferences.Editor editor = settings2.edit();
            editor.putString("ultimaAtualizacao", dateFormat.format(new Date()));
            editor.commit();
            Intent intent = new Intent("END_SYNC");
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);

            mergeRemote(provider, ultimaAtualizacao); //Atualiza os dados no DreamFactory

          /*  try {
                deleteDados(provider, ultimaAtualizacao);
            } catch (RemoteException e) {
                e.printStackTrace();
            }*/
        }
    }


    private void deleteDados(ContentProviderClient provider, String ultimaAtualizacao) throws RemoteException {

        Cursor c = provider.query(RealizadoContract.CONTENT_URI, null, RealizadoContract.UPDATED+"<?", new String[]{ultimaAtualizacao}, null);
        System.out.println("getCount-previstoCARAaqui1>>>"+c.getCount());
        while (c.moveToNext()) {
            System.out.println("IdObraaaaPREVISTO2222222=" + c.getLong(c.getColumnIndex(RealizadoContract.VISTORIA)));
            provider.delete(Uri.parse(RealizadoContract.CONTENT_URI + ""), "10", null);
        }

        }
    private void mergeLocal(ContentProviderClient provider, Map<String,String> obraList, String ultimaAtualizacao) throws ExecutionException, InterruptedException {
        ContentValues values = new ContentValues();
            if (obraList != null) {
            try {
                Cursor c = provider.query(ObraContract.CONTENT_URI, new String[]{ObraContract.CODIGO}, ObraContract.CODIGO+"=?", new String[]{String.valueOf(obraList.get("idObra")) }, null);
                values.clear();
                values.put(ObraContract.CONCLUIDA, 0);
                values.put(ObraContract.FORMALIZACAO, String.valueOf(obraList.get("formalizacao")));
                values.put(ObraContract.DATAINICIOOBRA, String.valueOf(obraList.get("dataInicioObra")));
                values.put(ObraContract.IDFORMALIZACAO, String.valueOf(obraList.get("idFormalizacao")));
                values.put(ObraContract.MODALIDADE, String.valueOf(obraList.get("modalidade")));
                values.put(ObraContract.IDMODALIDADE, String.valueOf(obraList.get("idModalidade")));
                values.put(ObraContract.MUNICIPIO, String.valueOf(obraList.get("municipio")));
                values.put(ObraContract.OBJETO, String.valueOf(obraList.get("objeto")));
                values.put(ObraContract.DESCRICAO, String.valueOf(obraList.get("descricao")));
                values.put(ObraContract.TIPOVISTORIA, String.valueOf(obraList.get("tipoVistoria")));
                values.put(ObraContract.UEE, String.valueOf(obraList.get("uee")));
                values.put(ObraContract.VALOROBRA, String.valueOf(obraList.get("valorObra")));
                values.put(ObraContract.UPDATED, String.valueOf(obraList.get("updatedAt")));
                values.put(ObraContract.EMAILFISCALELETRICO, String.valueOf(obraList.get("emailFiscalEletrico")));
                values.put(ObraContract.NOMEFISCALCIVIL, String.valueOf(obraList.get("nomeFiscalCivil")));
                values.put(ObraContract.NOMEFISCALELETRICO, String.valueOf(obraList.get("nomeFiscalEletrico")));
                values.put(ObraContract.CREAFISCALCIVIL, String.valueOf(obraList.get("creaFiscalCivil")));
                values.put(ObraContract.CREAFISCALELETRICO, String.valueOf(obraList.get("creaFiscalEletrico")));

                if (c.getCount() == 0) { //Se a obra ainda não existe no SQLite, Insert!
                    values.put(ObraContract.CODIGO, String.valueOf(obraList.get("idObra")));
                    provider.insert(ObraContract.CONTENT_URI, values);
                } else {                // Se já existe, Update!
                    c.moveToNext();
                    provider.update(Uri.parse(ObraContract.CONTENT_URI + "/" + c.getLong(0)), values, null, null);
                }
                c.close();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }


    }

    //TODO e se mudar o email?

    private void mergeRemote(ContentProviderClient provider, String ultimaAtualizacao) {
        System.out.println("Chegou ao mergeRemote");
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
        final String emailFiscal = settings.getString("Email", "");
        final String tipoFiscal = settings.getString("tipoFiscal", "civil");

        Cursor c = null;
        try {
            //obra
            /*provider.query --> Traz apenas as obras que sofreram alteração no SQLite interno para
              serem atualizados no DreamFactory.

              Para efeito de teste, substitua o ">" pelo "<", então, irá atualizar todas as Obras
              que não foram atualizadas, estando ou não no DreamFactory.
             */
            c = provider.query(ObraContract.CONTENT_URI, null, ObraContract.UPDATED+">?", new String[]{ultimaAtualizacao}, null);
            System.out.println("getCount-OBRA>>>"+c.getCount());
            while (c.moveToNext()) {

                StringBuilder whereSQL = new StringBuilder();
                whereSQL.append("idObra=" + c.getInt(c.getColumnIndex(ObraContract.CODIGO)));
                if (tipoFiscal.equals("civil")) {
                    whereSQL.append(" and ").append("emailFiscalCivil =\"" + emailFiscal + "\"");
                } else {
                    whereSQL.append(" and ").append("emailFiscalEletrico =\"" + emailFiscal + "\"");
                }

                AsyncTask records = todo.getDados("obra", whereSQL.toString()); //Busca dados do idObra enviado na query

                StringBuilder novoObjeto = new StringBuilder();
                novoObjeto.append("idObra="+ c.getString(c.getColumnIndex(ObraContract.CODIGO))).append(", ");
                novoObjeto.append("descricao="+ c.getString(c.getColumnIndex(ObraContract.DESCRICAO))).append(", ");
                novoObjeto.append("updatedAt="+ c.getString(c.getColumnIndex(ObraContract.UPDATED))).append(", ");

                if (tipoFiscal.equals("civil")) {
                    novoObjeto.append("emailFiscalCivil=" + emailFiscal);
                } else {
                    novoObjeto.append("emailFiscalEletrico=" + emailFiscal);
                }

                if (records.get().toString().equals("[]")) {    //Se o resultado da consulta for uma lista vazia... Create!
                    System.out.println("Entrou no create OBRA..............");
                    todo.createDados("obra", novoObjeto.toString());
                } else {
                    System.out.println("Entrou no updateeeeee OBRA..............");
                    todo.updateDados("obra", novoObjeto.toString(), whereSQL.toString());
                }

            }
            c.close();

            //previsto
            c = provider.query(PrevistoContract.CONTENT_URI, null, PrevistoContract.UPDATED+">?", new String[]{ultimaAtualizacao}, null);
            System.out.println("getCount-PREVISTO>>>"+c.getCount());
            while (c.moveToNext()) {
                long idObraPrevisto = c.getLong(c.getColumnIndex(PrevistoContract.OBRA));
                Cursor d = provider.query(Uri.parse(ObraContract.CONTENT_URI + "/" + idObraPrevisto), null, null, null, null);
                d.moveToNext();
                final int codigoObra = Integer.parseInt(d.getString(d.getColumnIndex(ObraContract.CODIGO)));
                d.close();
                final String descricaoPrevisto = c.getString(c.getColumnIndex(PrevistoContract.DESCRICAO));
                final double valorPrevisto = c.getFloat(c.getColumnIndex(PrevistoContract.VALOR));

                StringBuilder whereSQL = new StringBuilder();
                whereSQL.append("idObra=" + codigoObra);
                whereSQL.append(" and ").append("descricao =\"" + descricaoPrevisto + "\"");

                AsyncTask records = todo.getDados("previsto", whereSQL.toString());

                StringBuilder novoObjeto = new StringBuilder();
                novoObjeto.append("idObra="+ codigoObra).append(", ");
                novoObjeto.append("valor="+ valorPrevisto).append(", ");
                novoObjeto.append("descricao="+ descricaoPrevisto );

                System.out.println("Novo Objetooo: "+novoObjeto.toString());

                if (records.get().toString().equals("[]")) {
                    System.out.println("Entrou no create PREVISTO..............idObra:"+codigoObra);
                    todo.createDados("previsto", novoObjeto.toString());
                } else {
                   System.out.println("Entrou no updateeeeee PREVISTO..............idObra:"+codigoObra);
                   todo.updateDados("previsto", novoObjeto.toString(), whereSQL.toString());
                }
            }

            c.close();

            //vistoria - c = provider.query(VistoriaContract.CONTENT_URI, null, VistoriaContract.UPDATED+">?", new String[]{ultimaAtualizacao}, null);
            c = provider.query(VistoriaContract.CONTENT_URI, null, VistoriaContract.UPDATED+">?", new String[]{ultimaAtualizacao}, null);
            System.out.println("getCount-VISTORIA>>>"+c.getCount());
            while (c.moveToNext()) {
                final long idObraVistoria = c.getLong(c.getColumnIndex(VistoriaContract.OBRA));
                Cursor d = provider.query(Uri.parse(ObraContract.CONTENT_URI + "/" + idObraVistoria), null, null, null, null);
                d.moveToNext();
                final int codigoObra = Integer.parseInt(d.getString(d.getColumnIndex(ObraContract.CODIGO)));
                d.close();
                final String dataVistoria = c.getString(c.getColumnIndex(VistoriaContract.DATAVISTORIA));
                final String statusObra = c.getString(c.getColumnIndex(VistoriaContract.STATUSOBRA));
                final float valorMedicao = c.getFloat(c.getColumnIndex(VistoriaContract.VALORMEDICAO));
                final String comentarios = c.getString(c.getColumnIndex(VistoriaContract.COMENTARIOS));
                final int planilha = c.getInt(c.getColumnIndex(VistoriaContract.PLANILHA));
                final int diario = c.getInt(c.getColumnIndex(VistoriaContract.DIARIO));
                final int especificacoes = c.getInt(c.getColumnIndex(VistoriaContract.ESPECIFICACOES));
                final int projetos = c.getInt(c.getColumnIndex(VistoriaContract.PROJETOS));
                final String inicio = c.getString(c.getColumnIndex(VistoriaContract.INICIO));
                final String fim = c.getString(c.getColumnIndex(VistoriaContract.FIM));
                final String latitude = c.getString(c.getColumnIndex(VistoriaContract.LATITUDE));
                final String longitude = c.getString(c.getColumnIndex(VistoriaContract.LONGITUDE));

                // final String email_Fiscal = d.getString(c.getColumnIndex(VistoriaContract.EMAILFISCAL));

                StringBuilder whereSQL = new StringBuilder();
                whereSQL.append("idObra=" + codigoObra);

                AsyncTask records = todo.getDados("vistoria", whereSQL.toString());
                System.out.println(records.get().toString());

                StringBuilder novoObjeto = new StringBuilder();
                novoObjeto.append("idObra="+ codigoObra).append(", ");
                novoObjeto.append("dataVistoria="+ dataVistoria).append(", ");
                novoObjeto.append("statusObra="+ statusObra).append(", ");
                novoObjeto.append("valor="+ valorMedicao).append(", ");
                novoObjeto.append("comentarios="+ comentarios).append(", ");
                novoObjeto.append("planilha="+ planilha).append(", ");
                novoObjeto.append("diario="+ diario).append(", ");
                novoObjeto.append("especificacoes="+ especificacoes).append(", ");
                novoObjeto.append("projetos="+ projetos).append(", ");
                novoObjeto.append("inicio="+ inicio).append(", ");
                novoObjeto.append("fim="+ fim).append(", ");
                novoObjeto.append("latitude="+ latitude).append(", ");
                novoObjeto.append("longitude="+ longitude).append(", ");
                novoObjeto.append("tipoFiscal="+ tipoFiscal).append(", ");
                novoObjeto.append("emailFiscal="+ emailFiscal);

                if (records.get().toString().equals("[]")) {
                    System.out.println("Entrou no create......vistoria........id:"+codigoObra);
                    todo.createDados("vistoria", novoObjeto.toString());
                } else {
                    System.out.println("Entrou no updateeeeee.......vistoria.......id:"+codigoObra);
                    todo.updateDados("vistoria", novoObjeto.toString(), whereSQL.toString());
                }

            }
            c.close();


//        	realizado - c = provider.query(RealizadoContract.CONTENT_URI, null, RealizadoContract.UPDATED+">?", new String[]{ultimaAtualizacao}, null);
            c = provider.query(RealizadoContract.CONTENT_URI, null, RealizadoContract.UPDATED+">?", new String[]{ultimaAtualizacao}, null);
            System.out.println("getCount-REALIZADO>>>"+c.getCount());
            while (c.moveToNext()) {
                final int idVistoria = c.getInt(c.getColumnIndex(RealizadoContract.VISTORIA));
                final String descricao = c.getString(c.getColumnIndex(RealizadoContract.DESCRICAO));
                final float valor = c.getFloat(c.getColumnIndex(RealizadoContract.VALOR));
                final String avaliacao = c.getString(c.getColumnIndex(RealizadoContract.AVALIACAO));
                final String comentarios = c.getString(c.getColumnIndex(RealizadoContract.COMENTARIOS));

                System.out.println("idVistoria>>>>>"+ idVistoria);

                StringBuilder whereSQL = new StringBuilder();
                whereSQL.append("idVistoria=" + idVistoria);
                whereSQL.append(" and ").append("descricao =\"" + descricao + "\"");

                AsyncTask records = todo.getDados("realizado", whereSQL.toString());

                StringBuilder novoObjeto = new StringBuilder();
                novoObjeto.append("idVistoria="+ idVistoria).append(", ");
                novoObjeto.append("descricao="+ descricao).append(", ");
                novoObjeto.append("valor="+ valor).append(", ");
                novoObjeto.append("avaliacao="+ avaliacao).append(", ");
                novoObjeto.append("comentarios="+ comentarios);

                if (records.get().toString().equals("[]")) {
                    System.out.println("Entrou no create..............idVistoria:"+idVistoria);
                    todo.createDados("realizado", novoObjeto.toString());
                } else {
                    System.out.println("Entrou no updateeeeee..............idVistoria:"+idVistoria);
                    todo.updateDados("realizado", novoObjeto.toString(), whereSQL.toString());
                }
            }
            c.close();
//foto
            c = provider.query(FotoContract.CONTENT_URI, null, FotoContract.UPDATED+">?", new String[]{ultimaAtualizacao}, null);
            System.out.println("getCount-FOTO>>>"+c.getCount());
            while (c.moveToNext()) {
                final long idVistoria = c.getLong(c.getColumnIndex(FotoContract.VISTORIA));
                final String uri = c.getString(c.getColumnIndex(FotoContract.URI));
                final String comentario = c.getString(c.getColumnIndex(FotoContract.COMENTARIO));

                StringBuilder whereSQL = new StringBuilder();
                whereSQL.append("idVistoria=" + idVistoria);
                whereSQL.append(" and ").append("uri =\"" + uri + "\"");

                AsyncTask records = todo.getDados("foto", whereSQL.toString());

                StringBuilder novoObjeto = new StringBuilder();
                novoObjeto.append("idVistoria=" + idVistoria).append(", ");
                novoObjeto.append("uri=" + uri).append(", ");
                novoObjeto.append("comentario=" + comentario);

                if (records.get().toString().equals("[]")) {
                    System.out.println("Entrou no create..............idVistoria:" + idVistoria);
                    todo.createDados("foto", novoObjeto.toString());
                } else {
                    System.out.println("Entrou no updateeeeee..............idVistoria:" + idVistoria);
                    todo.updateDados("foto", novoObjeto.toString(), whereSQL.toString());
                }
            }
        } catch (RemoteException e1) {
            e1.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        c.close();
    }

}