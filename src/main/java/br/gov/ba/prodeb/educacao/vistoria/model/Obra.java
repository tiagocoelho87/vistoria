package br.gov.ba.prodeb.educacao.vistoria.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentMimeTypeVnd;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentUri;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultSortOrder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;

@DatabaseTable(tableName = Contract.ObraContract.TABLENAME)
@DefaultContentUri(authority=Contract.AUTHORITY, path=Contract.ObraContract.CONTENT_URI_PATH)
@DefaultContentMimeTypeVnd(name=Contract.ObraContract.MIMETYPE_NAME, type=Contract.ObraContract.MIMETYPE_TYPE)
public class Obra {
	
    @DatabaseField(columnName = BaseColumns._ID, generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false)
    @DefaultSortOrder
    private String codigo;
    
    @DatabaseField
	private String descricao;
    
    @DatabaseField
	private String dataInicioObra;

    @DatabaseField
	private String formalizacao;

    @DatabaseField
	private String idFormalizacao;

    @DatabaseField
	private String modalidade;

    @DatabaseField
	private String idModalidade;

    @DatabaseField
	private String municipio;

    @DatabaseField
	private String objeto;

    @DatabaseField
	private String tipoVistoria;

    @DatabaseField
	private String uee;
    
    @DatabaseField
	private float valorObra;

    @DatabaseField
	private String emailFiscalCivil;


    @DatabaseField
	private String creaFiscalCivil;

    @DatabaseField
	private String emailFiscalEletrico;


    @DatabaseField
	private String creaFiscalEletrico;

    @DatabaseField
	private String nomeFiscalCivil;

    @DatabaseField
	private String nomeFiscalEletrico;

    public String getDataInicioObra() {
		return dataInicioObra;
	}

	public void setDataInicioObra(String dataInicioObra) {
		this.dataInicioObra = dataInicioObra;
	}

	public String getFormalizacao() {
		return formalizacao;
	}

	public void setFormalização(String formalizacao) {
		this.formalizacao = formalizacao;
	}

	public String getIdFormalizacao() {
		return idFormalizacao;
	}

	public void setIdFormalizacao(String idFormalizacao) {
		this.idFormalizacao = idFormalizacao;
	}

	public String getIdModalidade() {
		return idModalidade;
	}

	public void setIdModalidade(String idModalidade) {
		this.idModalidade = idModalidade;
	}

	public String getModalidade() {
		return modalidade;
	}

	public void setModalidade(String modalidade) {
		this.modalidade = modalidade;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getObjeto() {
		return objeto;
	}

	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}

	public String getTipoVistoria() {
		return tipoVistoria;
	}

	public void setTipoVistoria(String tipoVistoria) {
		this.tipoVistoria = tipoVistoria;
	}

	public String getUee() {
		return uee;
	}

	public void setUee(String uee) {
		this.uee = uee;
	}

	public float getValorObra() {
		return valorObra;
	}

	public void setValorObra(float valorObra) {
		this.valorObra = valorObra;
	}

	public String getNomeFiscalCivil() {
		return nomeFiscalCivil;
	}

	public void setNomeFiscalCivil(String nomeFiscalCivil) {
		this.nomeFiscalCivil = nomeFiscalCivil;
	}

	public String getCreaFiscalCivil() {
		return creaFiscalCivil;
	}

	public void setCreaFiscalCivil(String creaFiscalCivil) {
		this.creaFiscalCivil = creaFiscalCivil;
	}

	public String getCreaFiscalEletrico() {
		return creaFiscalEletrico;
	}

	public void setCreaFiscalEletrico(String creaFiscalEletrico) {
		this.creaFiscalEletrico = creaFiscalEletrico;
	}

	public String getNomeFiscalEletrico() {
		return nomeFiscalEletrico;
	}

	public void setNomeFiscalEletrico(String nomeFiscalEletrico) {
		this.nomeFiscalEletrico = nomeFiscalEletrico;
	}

	public String getEmailFiscalCivil() {
		return emailFiscalCivil;
	}

	public void setEmailFiscalCivil(String emailFiscalCivil) {
		this.emailFiscalCivil = emailFiscalCivil;
	}

	public String getEmailFiscalEletrico() {
		return emailFiscalEletrico;
	}

	public void setEmailFiscalEletrico(String emailFiscalEletrico) {
		this.emailFiscalEletrico = emailFiscalEletrico;
	}

	@ForeignCollectionField
    private ForeignCollection<Previsto> servicos;
    
    @DatabaseField
    private boolean concluida = false;
    
    @DatabaseField
    private Date updatedAt;

    public Obra() {    	
    }
	
    public Obra(String codigo, String descricao) {
    	this.codigo =  codigo;
    	this.descricao = descricao;
    }

    public Obra(String codigo, String descricao, String tipoVistoria, String dataInicioObra, String objeto, String municipio, String uee, float valorObra, String emailFiscalCivil, String emailFiscalEletrico, String nomeFiscalCivil, String nomeFiscalEletrico, String creaFiscalCivil, String creaFiscalEletrico, String modalidade, String idModalidade, String formalizacao, String idFormalizacao) {
    	this.codigo =  codigo;
    	this.descricao = descricao;
    	this.tipoVistoria = tipoVistoria;
    	this.dataInicioObra = dataInicioObra;
    	this.objeto = objeto;
    	this.municipio = municipio;
    	this.uee = uee;
    	this.valorObra = valorObra;
    	this.emailFiscalCivil = emailFiscalCivil;
    	this.emailFiscalEletrico = emailFiscalEletrico;
    	this.nomeFiscalCivil = nomeFiscalCivil;      
    	this.nomeFiscalEletrico = nomeFiscalEletrico;
    	this.creaFiscalCivil = creaFiscalCivil;      
    	this.creaFiscalEletrico = creaFiscalEletrico;
    	this.modalidade = modalidade;
    	this.idModalidade = idModalidade;
    	this.formalizacao = formalizacao;
    	this.idFormalizacao = idFormalizacao;
    }

    public int getId() {
        return id;
    }

    public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

    public final boolean getConcluida() {
		return this.concluida;
	}
	
	public void setConcluida(boolean concluida) {
		this.concluida = concluida;
	}

    public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}	
	
    public ForeignCollection<Previsto> getPrevisto() {
        return servicos;
    }
    
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Obra other = (Obra) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Obra [codigo=" + codigo + ", descricao=" + descricao + "]";
	}

	public ContentValues getContentValues() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        ContentValues values = new ContentValues();
        values.put(ObraContract.CODIGO, codigo);
        values.put(ObraContract.DESCRICAO, descricao);
        values.put(ObraContract.UPDATED, dateFormat.format(new Date()));
        return values;
    }

    public static Obra fromCursor(Context context, Cursor c) {
    	String codigo = c.getString(c.getColumnIndex(ObraContract.CODIGO));
        String descricao = c.getString(c.getColumnIndex(ObraContract.DESCRICAO));
    	String tipoVistoria = c.getString(c.getColumnIndex(ObraContract.TIPOVISTORIA));
        String dataInicioObra = c.getString(c.getColumnIndex(ObraContract.DATAINICIOOBRA));
        String objeto = c.getString(c.getColumnIndex(ObraContract.OBJETO));
        String municipio = c.getString(c.getColumnIndex(ObraContract.MUNICIPIO));
        String uee = c.getString(c.getColumnIndex(ObraContract.UEE));
        float valorObra = c.getFloat(c.getColumnIndex(ObraContract.VALOROBRA));
        String emailFiscalCivil = c.getString(c.getColumnIndex(ObraContract.EMAILFISCALCIVIL));
        String emailFiscalEletrico = c.getString(c.getColumnIndex(ObraContract.EMAILFISCALELETRICO));
        String nomeFiscalCivil = c.getString(c.getColumnIndex(ObraContract.NOMEFISCALCIVIL));              
        String nomeFiscalEletrico = c.getString(c.getColumnIndex(ObraContract.NOMEFISCALELETRICO));
        String creaFiscalCivil = c.getString(c.getColumnIndex(ObraContract.CREAFISCALCIVIL));              
        String creaFiscalEletrico = c.getString(c.getColumnIndex(ObraContract.CREAFISCALELETRICO));
        String modalidade = c.getString(c.getColumnIndex(ObraContract.MODALIDADE));
        String idModalidade = c.getString(c.getColumnIndex(ObraContract.IDMODALIDADE));
        String formalizacao = c.getString(c.getColumnIndex(ObraContract.FORMALIZACAO));
        String idFormalizacao = c.getString(c.getColumnIndex(ObraContract.IDFORMALIZACAO));
        return new Obra(codigo, descricao, tipoVistoria, dataInicioObra, objeto, municipio, uee, valorObra, emailFiscalCivil, emailFiscalEletrico, nomeFiscalCivil, nomeFiscalEletrico, creaFiscalCivil, creaFiscalEletrico, modalidade, idModalidade, formalizacao, idFormalizacao);
    }
    
    

}
