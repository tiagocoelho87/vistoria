package br.gov.ba.prodeb.educacao.vistoria.adapter;

import android.content.Context;
import android.database.Cursor;
import android.text.InputFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.PrevistoContract;
import br.gov.ba.prodeb.educacao.vistoria.helper.DecimalDigitsInputFilter;
import br.gov.ba.prodeb.educacao.vistoria.helper.InputFilterMinMax;
import br.gov.ba.prodeb.educacao.vistoria.model.Previsto;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.res.StringArrayRes;

@EBean
public class PrevistoAdapter extends BaseAdapter {
	
	@StringArrayRes
	public String[] servicos;
	
	public float[] valores;

    @RootContext
    Context context;
    
	public void initAdapter() {
		if (valores == null) {
			 valores = new float[servicos.length];
		}
		for (int i = 0; i < servicos.length; i++) {
		  valores[i] = 0f;
		}
	}

	public void initAdapter(Cursor c) {
		if (valores == null) {
			 valores = new float[servicos.length];
		}
		for (int i = 0; i < servicos.length; i++) {
			  valores[i] = 0f;
			}
		while (c.moveToNext()) {
			for (int i = 0; i < servicos.length; i++) {
				if (servicos[i].equals(c.getString(c.getColumnIndex(PrevistoContract.DESCRICAO)))) {
					valores[i] = c.getFloat(c.getColumnIndex(PrevistoContract.VALOR));
					break;
				}
			}
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		PrevistoItemView previstoItemView;
        if (convertView == null) {
        	previstoItemView = PrevistoItemView_.build(context);
        } else {
        	previstoItemView = (PrevistoItemView) convertView;
        }
        
        previstoItemView.position = position;
        previstoItemView.adapter = this;
        
        previstoItemView.getValor().setFilters(new InputFilter[]{ new InputFilterMinMax("0.00", "100.00"), new DecimalDigitsInputFilter(3,2)});
                
        previstoItemView.bind(getItem(position));
        
//        previstoItemView.setId(position);
        
        return previstoItemView;
	}

	@Override
	public int getCount() {
		return servicos.length;
	}

	@Override
	public Previsto getItem(int position) {
		return new Previsto(servicos[position],valores[position],null);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

}
