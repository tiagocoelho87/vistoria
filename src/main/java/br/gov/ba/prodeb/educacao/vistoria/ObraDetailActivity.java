package br.gov.ba.prodeb.educacao.vistoria;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;

import com.google.analytics.tracking.android.EasyTracker;

import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.obra_detail_activity)
public class ObraDetailActivity extends Activity {
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			// If the screen is now in landscape mode, we can show the
			// dialog in-line with the list so we don't need this activity.
			finish();
			return;
		}
		
		if (savedInstanceState == null) {			
			ObraDetailFragment details = null;
			details = ObraDetailFragment_.builder()
					.build();
			getFragmentManager().beginTransaction()
					.add(R.id.obra_detail_container, details).commit();
		}
		
	}
	
    @Override
    public void onBackPressed() {
		ObraDetailFragment_ detail;
		detail = (ObraDetailFragment_) getFragmentManager().findFragmentById(R.id.obra_detail_container);
        detail.voltar();
    }

    @Override
    public void onResume() {
      super.onResume();
	  getActionBar().setTitle("Preparar Vistoria");
    }

    @Override
    public void onStart() {
      super.onStart();
      EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
      super.onStop();
      EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }

}
