package br.gov.ba.prodeb.educacao.vistoria.adapter;

import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;

public class LoaderObras extends CursorLoader {
	
	public LoaderObras(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	 @Override
	  public Cursor loadInBackground() {
		Cursor c =  this.getContext().getContentResolver().query(ObraContract.CONTENT_URI, null, ObraContract.CONCLUIDA + "=0", null, ObraContract.CODIGO);
	    return c;
	  }
	 
}
