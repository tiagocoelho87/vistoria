package br.gov.ba.prodeb.educacao.vistoria.syncadapter;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.dreamfactory.model.RecordResponse;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import br.gov.ba.prodeb.educacao.vistoria.ToDoDemoActivity;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;

public class VistoriaSyncAdapter2 extends AbstractThreadedSyncAdapter {

    private static final String TAG = "VistoriaSyncAdapter";

    private Context mContext;

    ToDoDemoActivity todo = new ToDoDemoActivity();

    public VistoriaSyncAdapter2(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContext = context;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public VistoriaSyncAdapter2(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContext = context;
    }



    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              final ContentProviderClient provider, SyncResult syncResult) {


        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
        String emailFiscal = settings.getString("Email", "");
        String tipoFiscal = settings.getString("tipoFiscal", "civil");
        final String ultimaAtualizacao = settings.getString("ultimaAtualizacao", "2001-01-01 00:00:00");
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date dtUltimaAtualizacao = null;
        try {
            dtUltimaAtualizacao = dateFormat.parse(ultimaAtualizacao);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (!(emailFiscal.isEmpty())) {

            ParseQuery<ParseObject> query = ParseQuery.getQuery("obra");
            query.whereGreaterThan("updatedAt", dtUltimaAtualizacao);

            StringBuilder whereSQL = new StringBuilder();
            whereSQL.append("updatedat <\"" + ultimaAtualizacao + "\"");

            if (tipoFiscal.equals("civil")) {
                whereSQL.append(" and ").append("emailFiscalCivil =\"" + emailFiscal + "\"");
                query.whereEqualTo("emailFiscalCivil", emailFiscal);
            } else {
                whereSQL.append(" and ").append("emailFiscalEletrico =\"" + emailFiscal + "\"");
                query.whereEqualTo("emailFiscalEletrico", emailFiscal);
            }
            //Linha deve ser excluida
            whereSQL.append(" and ").append("idObra = 3");

            AsyncTask records = todo.getDados("obra", whereSQL.toString());
            //List<RecordResponse> record2 = records.get();

            try {
               // List<RecordResponse> record2 = records.get();
                List<RecordResponse> record = new ArrayList<RecordResponse>();
                System.out.println("Registrosssss11 ---> " + records.get().toString());

                String value = records.get().toString();
                String[] arrValue = value.split(",");
                Map<String,String> valueMap = new HashMap<String, String>();
                for (String string : arrValue) {
                    String[] mapPair = string.split(":");
                    valueMap.put(mapPair[0].replace("[","").replace("]","").trim(), mapPair[1].replace("[","").replace("]","").trim());
                }

               /* List<RecordResponse> record = new ArrayList<RecordResponse>();*/

                if ( record != null) {
                   //mergeLocal(provider, record, ultimaAtualizacao);
                    mergeLocal(provider, valueMap, ultimaAtualizacao);
                    query.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        //public void done(RecordResponse obraList, ApiException e) {
                        public void done(List<ParseObject> obraList, ParseException e) {
                            if (e == null) {
                              //  mergeLocal(provider, obraList, ultimaAtualizacao);
                            } else {
                                Log.d("obra", "Error: " + e.getMessage());
                            }
                        }
                    });
                }
            } catch (Exception e) { e.printStackTrace(); }

            System.out.println("PASSSOUUUUUUUUUUUUUUUUUU Mesmo? " + dtUltimaAtualizacao.toString());
            mergeRemote(provider, ultimaAtualizacao);
        }

    }

    private void mergeLocal(ContentProviderClient provider, Map<String,String> obraList, String ultimaAtualizacao) throws ExecutionException, InterruptedException {
    //private void mergeLocal(ContentProviderClient provider, List<RecordResponse> obraList, String ultimaAtualizacao) {
        ContentValues values = new ContentValues();

       // for (Map<String,String> localObra: obraList) {
            if (obraList != null) {
        //for (RecordResponse localObra : obraList) {
            try {
                Cursor c = provider.query(ObraContract.CONTENT_URI, new String[]{ObraContract.CODIGO}, ObraContract.CODIGO+"=?", new String[]{String.valueOf(obraList.get("idObra"))/*localObra.getIdObra()*/ }, null);
                values.clear();
                values.put(ObraContract.CONCLUIDA, 0);
                        //System.out.println("getformalizacao dentro do merge" +localObra.getFormalizacao());
                       /* values.put(ObraContract.FORMALIZACAO, String.valueOf(localObra.get("formalizacao")));
                       // values.put(ObraContract.DATAINICIOOBRA, localObra.getDataInicioObra());
                        values.put(ObraContract.IDFORMALIZACAO, localObra.getIdFormalizacao());
                        values.put(ObraContract.MODALIDADE, localObra.getModalidade());
                        values.put(ObraContract.IDMODALIDADE, localObra.getIdModalidade());
                        values.put(ObraContract.MUNICIPIO, localObra.getMunicipio());
                        values.put(ObraContract.OBJETO, localObra.getObjeto());
                        values.put(ObraContract.TIPOVISTORIA, localObra.getTipoVistoria());
                        values.put(ObraContract.UEE, localObra.getUee());
                       // values.put(ObraContract.VALOROBRA, localObra.getValorObra());
                        values.put(ObraContract.EMAILFISCALCIVIL, localObra.getEmailFiscalCivil());
                        values.put(ObraContract.EMAILFISCALELETRICO, localObra.getEmailFiscalEletrico());
                        values.put(ObraContract.NOMEFISCALCIVIL, localObra.getNomeFiscalCivil());
                        values.put(ObraContract.NOMEFISCALELETRICO, localObra.getNomeFiscalEletrico());
                        values.put(ObraContract.CREAFISCALCIVIL, localObra.getCreaFiscalCivil());
                        values.put(ObraContract.CREAFISCALELETRICO, localObra.getCreaFiscalEletrico());*/

System.out.println("Obraaaaaaaaaaaaaaaa="+String.valueOf(obraList.get("idObra")));
System.out.println("Municipio="+String.valueOf(obraList.get("municipio")));
                values.put(ObraContract.FORMALIZACAO, String.valueOf(obraList.get("formalizacao")));
                values.put(ObraContract.DATAINICIOOBRA, String.valueOf(obraList.get("dataInicioObra")));
                values.put(ObraContract.IDFORMALIZACAO, String.valueOf(obraList.get("idFormalizacao")));
                values.put(ObraContract.MODALIDADE, String.valueOf(obraList.get("modalidade")));
                values.put(ObraContract.IDMODALIDADE, String.valueOf(obraList.get("idModalidade")));
                values.put(ObraContract.MUNICIPIO, "Salvadorrrr"); //String.valueOf(obraList.get("municipio"))
                values.put(ObraContract.OBJETO, String.valueOf(obraList.get("objeto")));
                values.put(ObraContract.TIPOVISTORIA, String.valueOf(obraList.get("tipoVistoria")));
                values.put(ObraContract.UEE, String.valueOf(obraList.get("uee")));
                values.put(ObraContract.VALOROBRA, String.valueOf(obraList.get("valorObra")));
                values.put(ObraContract.EMAILFISCALCIVIL, String.valueOf(obraList.get("emailFiscalCivil")));
                values.put(ObraContract.EMAILFISCALELETRICO, String.valueOf(obraList.get("emailFiscalEletrico")));
                values.put(ObraContract.NOMEFISCALCIVIL, String.valueOf(obraList.get("nomeFiscalCivil")));
                values.put(ObraContract.NOMEFISCALELETRICO, String.valueOf(obraList.get("nomeFiscalEletrico")));
                values.put(ObraContract.CREAFISCALCIVIL, String.valueOf(obraList.get("creaFiscalCivil")));
                values.put(ObraContract.CREAFISCALELETRICO, String.valueOf(obraList.get("creaFiscalEletrico")));

                if (c.getCount() == 0) {
                    values.put(ObraContract.CODIGO, String.valueOf(obraList.get("idObra")));
                   // values.put(ObraContract.CODIGO, localObra.getIdObra());
                    provider.insert(ObraContract.CONTENT_URI, values);
                } else {
                    c.moveToNext();
                    provider.update(Uri.parse(ObraContract.CONTENT_URI + "/" + c.getLong(0)), values, null, null);
                }
                c.close();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("ultimaAtualizacao", dateFormat.format(new Date()));
        editor.commit();
        Intent intent = new Intent("END_SYNC");
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }

    //TODO e se mudar o email?

    private void mergeRemote(ContentProviderClient provider, String ultimaAtualizacao) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getContext());
        final String emailFiscal = settings.getString("Email", "");
        final String tipoFiscal = settings.getString("tipoFiscal", "civil");
        Cursor c = null;
        try {
            //obra
            c = provider.query(ObraContract.CONTENT_URI, null, ObraContract.UPDATED+">?", new String[]{ultimaAtualizacao}, null);
            while (c.moveToNext()) {
                final String descricao = c.getString(c.getColumnIndex(ObraContract.DESCRICAO));
                ParseQuery<ParseObject> query = ParseQuery.getQuery("obra");
                query.whereEqualTo("idObra", c.getInt(c.getColumnIndex(ObraContract.CODIGO)));

                StringBuilder whereSQL = new StringBuilder();
                whereSQL.append("idObra=" + c.getInt(c.getColumnIndex(ObraContract.CODIGO)));

                if (tipoFiscal.equals("civil")) {
                    whereSQL.append(" and ").append("emailFiscalCivil =\"" + emailFiscal + "\"");
                    query.whereEqualTo("emailFiscalCivil", emailFiscal);
                } else {
                    whereSQL.append(" and ").append("emailFiscalEletrico =\"" + emailFiscal + "\"");
                    query.whereEqualTo("emailFiscalEletrico", emailFiscal);
                }
                //AsyncTask records = todo.getDados("obra", whereSQL.toString());
                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> obraList, ParseException e) {
                        if (e == null) {
                            if (obraList.size() > 0) {
                                //RecordRequest
                                //obraList.setDescricao(descricao);
                                obraList.get(0).put("descricao", descricao);
                                //records.execute();
                                obraList.get(0).saveInBackground();
                            }
                        } else {
                            Log.d("obra", "Error: " + e.getMessage());
                        }
                    }
                });
            }
            c.close();

        } catch (RemoteException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        c.close();
    }

}
