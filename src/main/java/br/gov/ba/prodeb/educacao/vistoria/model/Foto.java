package br.gov.ba.prodeb.educacao.vistoria.model;

import java.util.Date;

import android.provider.BaseColumns;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentMimeTypeVnd;
import com.tojc.ormlite.android.annotation.AdditionalAnnotation.DefaultContentUri;

@DatabaseTable(tableName = Contract.FotoContract.TABLENAME)
@DefaultContentUri(authority=Contract.AUTHORITY, path=Contract.FotoContract.CONTENT_URI_PATH)
@DefaultContentMimeTypeVnd(name=Contract.FotoContract.MIMETYPE_NAME, type=Contract.FotoContract.MIMETYPE_TYPE)
public class Foto {

	@DatabaseField(columnName = BaseColumns._ID, generatedId = true)
	private int id;
	
	@DatabaseField(canBeNull = false)
	private String uri;

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@DatabaseField
	private String comentario;
	
	@DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = "vistoria_id")
    private Vistoria vistoria;
	
    @DatabaseField
    private Date updatedAt;

    public Foto() {
    	
    }
    
    public Foto(String uri, String comentario) {
        this.uri = uri;
        this.comentario = comentario;
    }

    public int getId() {
    	return id;
    }

    public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

    public final String getComentario() {
		return this.comentario;
	}
	
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Vistoria getVistoria() {
		return this.vistoria;
	}
	
	public void setVistoria(Vistoria vistoria) {
		this.vistoria = vistoria;
	}

}
