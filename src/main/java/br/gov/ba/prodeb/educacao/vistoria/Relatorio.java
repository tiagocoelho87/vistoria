package br.gov.ba.prodeb.educacao.vistoria;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import br.gov.ba.prodeb.educacao.vistoria.helper.Utils;

import crl.android.pdfwriter.PDFWriter;
import crl.android.pdfwriter.PaperSize;
import crl.android.pdfwriter.StandardFonts;
import crl.android.pdfwriter.Transformation;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.TextView;
import android.widget.Toast;

@EBean
public class Relatorio {
	
	@RootContext
	Context context;
	
	@Bean
	VistoriaAtual vistoriaAtual;
	
	@Bean
    Utils utils;
	
	private String generatePDF() {
		PDFWriter mPDFWriter = new PDFWriter();

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		Bitmap logo = null;
		
		double andamento = 0;
		
        AssetManager mngr = context.getAssets();
		try {
			logo = BitmapFactory.decodeStream(mngr.open("logo.png"));
	        mPDFWriter.addImageKeepRatio(10, 792, 30, 30, logo);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
        mPDFWriter.setFont(StandardFonts.SUBTYPE, StandardFonts.HELVETICA_BOLD, StandardFonts.WIN_ANSI_ENCODING);
        mPDFWriter.addText(150, 820, 10, "Superintendência de Organização e Atendimento a Rede Escolar");
        mPDFWriter.addText(150, 808, 10, "Diretoria de Projetos Obras e Manutenção");
        mPDFWriter.addText(150, 796, 10, "Coordenação de Rede Física");
        mPDFWriter.addText(10, 760, 14, "RELATÓRIO DE VISTORIA DE OBRAS");

        mPDFWriter.addLine(10, 750, 565, 750);

        mPDFWriter.setFont(StandardFonts.SUBTYPE, StandardFonts.HELVETICA, StandardFonts.WIN_ANSI_ENCODING);

        mPDFWriter.addText(10, 720, 10, "Tipo de Vistoria: " + vistoriaAtual.obra.getTipoVistoria());
        mPDFWriter.addText(180, 720, 10, "Data da Visita: " + sdf.format(vistoriaAtual.vistoria.getDataVistoria()));
        mPDFWriter.addText(320, 720, 10, "Objeto: " + vistoriaAtual.obra.getObjeto());

        mPDFWriter.addLine(10, 710, 565, 710);

        mPDFWriter.addText(10, 690, 10, "Município: " + vistoriaAtual.obra.getMunicipio());
        mPDFWriter.addText(180, 690, 10, "U.E.E: " + vistoriaAtual.obra.getUee());

        mPDFWriter.addLine(10, 680, 565, 680);

        mPDFWriter.addText(10, 660, 10, "Modalidade: " + vistoriaAtual.obra.getModalidade() + " / "+ vistoriaAtual.obra.getIdModalidade());
        mPDFWriter.addText(320, 660, 10, "Formalização: " + vistoriaAtual.obra.getFormalizacao() + " / "+ vistoriaAtual.obra.getIdFormalizacao());

        mPDFWriter.addLine(10, 650, 565, 650);

    	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
    	String tipoFiscal = settings.getString("tipoFiscal", "civil");

        String fiscal = "";
        String crea = ""; 
        if (tipoFiscal.equals("eletrico")) {
        	fiscal = vistoriaAtual.obra.getNomeFiscalEletrico();
        	crea = vistoriaAtual.obra.getCreaFiscalEletrico();
        } else {
        	fiscal = vistoriaAtual.obra.getNomeFiscalCivil();  
        	crea = vistoriaAtual.obra.getCreaFiscalCivil();
        }
        
        if (crea == null) {
        	crea = "";
        }
        
        mPDFWriter.addText(10, 630, 10, "Técnico Fiscal: " + fiscal + "   CREA:" + crea);

        mPDFWriter.addLine(10, 620, 565, 620);

        mPDFWriter.addText(10, 600, 10, "Data de Início da Obra: " + vistoriaAtual.obra.getDataInicioObra());
        mPDFWriter.addText(180, 600, 10, "Valor da Obra: R$ " + vistoriaAtual.obra.getValorObra());

        mPDFWriter.addLine(10, 590, 565, 590);

        mPDFWriter.addText(10, 570, 10, "Status da Obra: " + vistoriaAtual.vistoria.getStatusObra());
        mPDFWriter.addText(180, 570, 10, "Valor da Medição: R$ " + vistoriaAtual.vistoria.getValorMedicao());

        mPDFWriter.addLine(10, 560, 565, 560);
        
        mPDFWriter.addText(10, 520, 10, "Serviço");
        mPDFWriter.addText(320, 520, 10, "% previsto");
        mPDFWriter.addText(380, 520, 10, "% executado");
        mPDFWriter.addText(460, 520, 10, "Avaliação Qualitativa");

        mPDFWriter.addLine(10, 510, 565, 510);

        int linha = 490;
        for (int i=0; i < vistoriaAtual.servicos.size(); i++) {
            mPDFWriter.addText(10, linha, 10, vistoriaAtual.servicos.get(i));
            mPDFWriter.addText(330, linha, 10, vistoriaAtual.previsto.get(i) +  "%");
            mPDFWriter.addText(390, linha, 10, vistoriaAtual.realizado.get(i) + "%");
            andamento += (vistoriaAtual.previsto.get(i) * vistoriaAtual.realizado.get(i));
            mPDFWriter.addText(465, linha, 10, vistoriaAtual.avaliacao.get(i));
            linha -=20;
            int quebras = 0;
            String print = vistoriaAtual.comentario.get(i);
            quebras = (print.length() / 100) + 1;
            for (int j=0;j<quebras;j++) {
            	String saida = "";
            	if (print.length() < 100) {
            		saida = print;
            	} else {
            		saida = print.substring(0, print.lastIndexOf(" ", 100));
            		print = print.substring(print.lastIndexOf(" ", 100)+1, print.length());
            	}
            	mPDFWriter.addText(10, linha, 10, saida);        	
            	linha -=10;
            }
        	mPDFWriter.addLine(10, linha, 565, linha);
            linha -=20;
            if (linha < 80) {
                mPDFWriter.newPage();
                
    	        mPDFWriter.addImageKeepRatio(10, 792, 30, 30, logo);

                mPDFWriter.setFont(StandardFonts.SUBTYPE, StandardFonts.HELVETICA_BOLD, StandardFonts.WIN_ANSI_ENCODING);
                mPDFWriter.addText(150, 820, 10, "Superintendência de Organização e Atendimento a Rede Escolar");
                mPDFWriter.addText(150, 808, 10, "Diretoria de Projetos Obras e Manutenção");
                mPDFWriter.addText(150, 796, 10, "Coordenação de Rede Física");
                mPDFWriter.addText(10, 760, 14, "RELATÓRIO DE VISTORIA DE OBRAS");
            	
                mPDFWriter.addText(10, 720, 10, "Serviço");
                mPDFWriter.addText(320, 720, 10, "% previsto");
                mPDFWriter.addText(380, 720, 10, "% executado");
                mPDFWriter.addText(460, 720, 10, "Avaliação Qualitativa");

                mPDFWriter.addLine(10, 710, 565, 710);
                
                linha = 690;
                
                mPDFWriter.setFont(StandardFonts.SUBTYPE, StandardFonts.HELVETICA, StandardFonts.WIN_ANSI_ENCODING);

            }
        }

        if (linha < 80) {
            mPDFWriter.newPage();
            
	        mPDFWriter.addImageKeepRatio(10, 792, 30, 30, logo);

            mPDFWriter.setFont(StandardFonts.SUBTYPE, StandardFonts.HELVETICA_BOLD, StandardFonts.WIN_ANSI_ENCODING);
            mPDFWriter.addText(150, 820, 10, "Superintendência de Organização e Atendimento a Rede Escolar");
            mPDFWriter.addText(150, 808, 10, "Diretoria de Projetos Obras e Manutenção");
            mPDFWriter.addText(150, 796, 10, "Coordenação de Rede Física");
            mPDFWriter.addText(10, 760, 14, "RELATÓRIO DE VISTORIA DE OBRAS");
            
            linha = 720;
        }
        
        mPDFWriter.setFont(StandardFonts.SUBTYPE, StandardFonts.HELVETICA_BOLD, StandardFonts.WIN_ANSI_ENCODING);

        mPDFWriter.addText(10, linha, 10, "TOTAL DO % EXECUTADO ACUMULADO DA OBRA: " + (andamento/100) + "%");
        linha -=10;
        mPDFWriter.addLine(10, linha, 565, linha);
        mPDFWriter.setFont(StandardFonts.SUBTYPE, StandardFonts.HELVETICA, StandardFonts.WIN_ANSI_ENCODING);
        linha -=40;
        mPDFWriter.addLine(10, linha, 565, linha);
        linha -=20;
        String existe = "";
        if (vistoriaAtual.vistoria.getProjetos()) existe += "projetos, ";
        if (vistoriaAtual.vistoria.getEspecificacoes()) existe += "especificações, ";
        if (vistoriaAtual.vistoria.getPlanilha()) existe += "planilha, ";
        if (vistoriaAtual.vistoria.getDiario()) existe += "diário de obras atualizado, ";
        if (existe.isEmpty()) {
        	existe = "nada consta";
        } else {
        	existe = existe.substring(0, existe.length() - 2);
        }
        mPDFWriter.addText(10, linha, 10, "Existe no barracão da obra: " + existe);
        linha -=10;
        mPDFWriter.addLine(10, linha, 565, linha);
        linha -=20;
        mPDFWriter.addText(10, linha, 10, "Comentários gerais sobre a obra:");
        linha -=30;
        int quebras = 0;
        String print = vistoriaAtual.vistoria.getComentarios();
        quebras = (print.length() / 100) + 1;
        for (int j=0;j<quebras;j++) {
        	String saida = "";
        	if (print.length() < 100) {
        		saida = print;
        	} else {
        		saida = print.substring(0, print.lastIndexOf(" ", 100));
        		print = print.substring(print.lastIndexOf(" ", 100)+1, print.length());
        	}
        	mPDFWriter.addText(10, linha, 10, saida);        	
        	linha -=10;
        }

        linha -=40;
        mPDFWriter.addLine(10, linha, 565, linha);

        Bitmap image;
        
        for (int i=0; i<utils.getFilePaths().size(); i++) {
            mPDFWriter.newPage();
            
	        mPDFWriter.addImageKeepRatio(10, 792, 30, 30, logo);

            mPDFWriter.setFont(StandardFonts.SUBTYPE, StandardFonts.HELVETICA_BOLD, StandardFonts.WIN_ANSI_ENCODING);
            mPDFWriter.addText(150, 820, 10, "Superintendência de Organização e Atendimento a Rede Escolar");
            mPDFWriter.addText(150, 808, 10, "Diretoria de Projetos Obras e Manutenção");
            mPDFWriter.addText(150, 796, 10, "Coordenação de Rede Física");
            mPDFWriter.addText(10, 760, 14, "RELATÓRIO DE VISTORIA DE OBRAS - ANEXOS");

            image = decodeFile(utils.getFilePaths().get(i), 200, 150);
        	mPDFWriter.addImage(30, 250, image);
        	
            mPDFWriter.setFont(StandardFonts.SUBTYPE, StandardFonts.HELVETICA, StandardFonts.WIN_ANSI_ENCODING);
            quebras = 0;
            print = vistoriaAtual.comentariosFotos.get(i);
            quebras = (print.length() / 100) + 1;
            linha = 200;
            for (int j=0;j<quebras;j++) {
            	String saida = "";
            	if (print.length() < 100) {
            		saida = print;
            	} else {
            		saida = print.substring(0, print.lastIndexOf(" ", 100));
            		print = print.substring(print.lastIndexOf(" ", 100)+1, print.length());
            	}
            	mPDFWriter.addText(30, linha, 10, saida);        	
            	linha -=10;
            }

        }

        int pageCount = mPDFWriter.getPageCount();
        for (int i = 0; i < pageCount; i++) {
        	mPDFWriter.setCurrentPage(i);
        	mPDFWriter.addText(10, 10, 8, Integer.toString(i + 1) + " / " + Integer.toString(pageCount));
        }
        
        String s = mPDFWriter.asString();
        return s;
	}
	
	private void outputToFile(String fileName, String pdfContent, String encoding) {
        File newFile = new File(Environment.getExternalStorageDirectory() + "/" + fileName);
        try {
            newFile.createNewFile();
            try {
            	FileOutputStream pdfFile = new FileOutputStream(newFile);
            	pdfFile.write(pdfContent.getBytes(encoding));
                pdfFile.close();
            } catch(FileNotFoundException e) {
            	//
            }
        } catch(IOException e) {
        	//
        }
	}
	
    public Uri generate() {
        String pdfcontent = generatePDF();
        outputToFile("relatorio.pdf",pdfcontent,"ISO-8859-1");
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "relatorio.pdf");
        return Uri.fromFile(file);
    }

    /*
     * Resizing image size
     */
    public static Bitmap decodeFile(String filePath, int WIDTH, int HIGHT) {
        try {
 
            File f = new File(filePath);
 
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
 
            final int REQUIRED_WIDTH = WIDTH;
            final int REQUIRED_HIGHT = HIGHT;
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_WIDTH
                    && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                scale *= 2;
 
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}
