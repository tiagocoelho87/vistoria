package br.gov.ba.prodeb.educacao.vistoria;

import br.gov.ba.prodeb.educacao.vistoria.adapter.LoaderObrasVistorias;
import br.gov.ba.prodeb.educacao.vistoria.adapter.ObraAdapter;
import br.gov.ba.prodeb.educacao.vistoria.adapter.ObraVistoriaAdapter;
import br.gov.ba.prodeb.educacao.vistoria.adapter.VistoriaAdapter;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.VistoriaContract;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;

import br.gov.ba.prodeb.educacao.vistoria.syncadapter.SyncUtils;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.app.ListFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

@EFragment
public class RelatorioListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
		
	@App
	VistoriaApplication vistoriaApp;

	@InstanceState
	int mCurCheckPosition = ListView.INVALID_POSITION;
	
	public static final int LOADER_ID = 1;
	
	private ObraVistoriaAdapter adapter;
	
    @Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		getListView().setBackgroundColor(getResources().getColor(R.color.cinzaclaro_listview));
		getListView().setSelector(R.drawable.list_selectors);

		setEmptyText("Nenhuma vistoria");	
		setListShown(false);
		adapter = new ObraVistoriaAdapter(this.getActivity(), null, 0);
        setListAdapter(adapter);
		getLoaderManager().initLoader(LOADER_ID, null, this);

	}
    
//	@ItemClick
//	public void listItemClicked(int position) {
//		mCurCheckPosition = position;
//		showDetails();		
//	}
	
	void showDetails() {
		if (vistoriaApp.isDualPane()) {
			getListView().setItemChecked(mCurCheckPosition, true);

			RelatorioDetailFragment details = (RelatorioDetailFragment_) getFragmentManager()
					.findFragmentById(R.id.relatorio_detail_container);
			if (details == null) {

				details = RelatorioDetailFragment_.builder()
						.build();

				FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				ft.replace(R.id.relatorio_detail_container, details);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commit();
				
			} else {
				if (details.isDetached()) {
					FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
					ft.attach(details);
					ft.commit();
				}
				details.configure();
			}
			
		}

	}
		
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		CursorLoader cursorLoader = new CursorLoader(getActivity(), VistoriaContract.CONTENT_URI, new String[]{VistoriaContract._ID, VistoriaContract.DATAVISTORIA, VistoriaContract.OBRA}, VistoriaContract.CONCLUIDA + "=1", null,
				VistoriaContract.OBRA + " ASC, " + VistoriaContract.DATAVISTORIA + " DESC");
		return cursorLoader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
		adapter.swapCursor(cursor);
        setListShown(true);
		
	}

	@Override
	public void onLoaderReset(Loader<Cursor> cursorLoader) {
//        setListShown(false);		
        adapter.swapCursor(null);
	}
    
    @Override
    public void onPause() {
      super.onPause();
    }
    
    @Override
    public void onResume() {
      super.onResume();
		getListView().setItemChecked(mCurCheckPosition, true);
    }
    
    @Override
    public void onStop() {
      super.onStop();
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

}
