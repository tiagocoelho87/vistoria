package br.gov.ba.prodeb.educacao.vistoria;

import android.app.Activity;
import android.support.v4.app.NavUtils;

import com.google.analytics.tracking.android.EasyTracker;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;

@EActivity(R.layout.obra_list_activity)
@OptionsMenu(R.menu.obra_list_activity)
public class ObraListActivity extends Activity {

	@App
	VistoriaApplication vistoriaApp;

//	private static String TAG="Obra";
	
    @OptionsItem(R.id.obra_inserir)
    void novaObra() {
    	ObraListFragment listFragment = (ObraListFragment_) this.getFragmentManager().findFragmentById(R.id.obra_list);
    	vistoriaApp.setObraId(0);
    	listFragment.showDetails();
    }

    @Override
    public void onResume() {
      super.onResume();
		getActionBar().setTitle("Preparar Vistoria");
		getActionBar().setSubtitle("Selecione um ítem");
    }

    @Override
    public void onStart() {
      super.onStart();
      EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
      super.onStop();
      EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    public void onBackPressed() {
		ObraDetailFragment details = (ObraDetailFragment_) getFragmentManager()
				.findFragmentById(R.id.obra_detail_container);
		if (details != null) {
			details.voltar();
		} else {
	    	NavUtils.navigateUpFromSameTask(this);			
		}
    }

 }
