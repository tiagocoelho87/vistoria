package br.gov.ba.prodeb.educacao.vistoria;

import android.app.Fragment;
import android.database.Cursor;
import android.widget.GridView;
import br.gov.ba.prodeb.educacao.vistoria.adapter.RealizadoAdapter;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.PrevistoContract;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

@EFragment(R.layout.vistoria_detail_fragment_servicos)
public class VistoriaDetailFragmentServicos extends Fragment {

		    
	@ViewById
    GridView listServicos;
	
	@Bean
	RealizadoAdapter realizadoAdapter;
	
	@Bean
	VistoriaAtual vistoriaAtual;
		
	@Override
	public void onResume () {
		super.onResume();
		Cursor c = this.getActivity().getContentResolver().query(PrevistoContract.CONTENT_URI, null, PrevistoContract.OBRA + "=?", new String[] {String.valueOf(vistoriaAtual.obraId)}, PrevistoContract.DESCRICAO);
		realizadoAdapter.initAdapter(c);
		c.close();
		listServicos.setAdapter(realizadoAdapter);
	}

	@Override
	public void onPause () {
		super.onPause();
	}
	
	@Override
	public void onDetach() {
	    super.onDetach();
	}

}

