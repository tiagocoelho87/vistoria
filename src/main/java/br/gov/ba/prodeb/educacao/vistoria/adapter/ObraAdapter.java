package br.gov.ba.prodeb.educacao.vistoria.adapter;

import br.gov.ba.prodeb.educacao.vistoria.R;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class ObraAdapter extends CursorAdapter {
 
	public ObraAdapter(Context context, Cursor c, int flags) {
		super(context, c, flags);
		// TODO Auto-generated constructor stub
	}

	@Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // when the view will be created for first time,
        // we need to tell the adapters, how each item will look
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View retView = inflater.inflate(R.layout.obra_list_item, parent, false);
 
        return retView;
    }
 
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView codigo = (TextView) view.findViewById(R.id.codigo);
        codigo.setText(cursor.getString(cursor.getColumnIndex(ObraContract.CODIGO)));
 
        TextView descricao = (TextView) view.findViewById(R.id.descricao);
        descricao.setText(cursor.getString(cursor.getColumnIndex(ObraContract.DESCRICAO)));
    }
    
        
}

