package br.gov.ba.prodeb.educacao.vistoria.contentprovider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class Contract {
	public static final String DATABASE_NAME = "Vistorias";
    public static final int DATABASE_VERSION = 1;

    public static final String AUTHORITY = "br.gov.ba.prodeb.educacao.vistoria.contentprovider";

    public static class ObraContract implements BaseColumns
    {
        public static final String TABLENAME = "obras";

        public static final String CONTENT_URI_PATH = TABLENAME;

        public static final String MIMETYPE_TYPE = TABLENAME;
        public static final String MIMETYPE_NAME = AUTHORITY + ".provider";

        // field info
        public static final String CODIGO = "codigo";
        public static final String DESCRICAO = "descricao";
        public static final String SERVICOS = "servicos";
        public static final String CONCLUIDA = "concluida";
        public static final String UPDATED = "updatedAt";
        public static final String DATAINICIOOBRA = "dataInicioObra";
        public static final String FORMALIZACAO = "formalizacao";
        public static final String IDFORMALIZACAO = "idFormalizacao";
        public static final String MODALIDADE = "modalidade";
        public static final String IDMODALIDADE = "idModalidade";
        public static final String MUNICIPIO = "municipio";
        public static final String OBJETO = "objeto";
        public static final String TIPOVISTORIA = "tipoVistoria";
        public static final String UEE = "uee";
        public static final String VALOROBRA = "valorObra";
        public static final String EMAILFISCALCIVIL = "emailFiscalCivil";
        public static final String EMAILFISCALELETRICO = "emailFiscalEletrico";
        public static final String NOMEFISCALCIVIL = "nomeFiscalCivil";
        public static final String NOMEFISCALELETRICO = "nomeFiscalEletrico";
        public static final String CREAFISCALCIVIL = "creaFiscalCivil";
        public static final String CREAFISCALELETRICO = "creaFiscalEletrico";

        // content uri pattern code
        public static final int CONTENT_URI_PATTERN_MANY = 1;
        public static final int CONTENT_URI_PATTERN_ONE = 2;

        public static final Uri CONTENT_URI = new Uri.Builder()
            .scheme(ContentResolver.SCHEME_CONTENT)
            .authority(AUTHORITY)
            .appendPath(CONTENT_URI_PATH)
            .build();
    }
    public static class PrevistoContract implements BaseColumns
    {
        public static final String TABLENAME = "previsto";

        public static final String CONTENT_URI_PATH = TABLENAME;

        public static final String MIMETYPE_TYPE = TABLENAME;
        public static final String MIMETYPE_NAME = AUTHORITY + ".provider";

        // field info
        public static final String DESCRICAO = "descricao";
        public static final String VALOR = "valor";
        public static final String OBRA = "obra_id";
        public static final String UPDATED = "updatedAt";

        // content uri pattern code
        public static final int CONTENT_URI_PATTERN_MANY = 3;
        public static final int CONTENT_URI_PATTERN_ONE = 4;

        // Refer to activity.
        public static final Uri CONTENT_URI = new Uri.Builder()
            .scheme(ContentResolver.SCHEME_CONTENT)
            .authority(AUTHORITY)
            .appendPath(CONTENT_URI_PATH)
            .build();
    }
    public static class VistoriaContract implements BaseColumns
    {
        public static final String TABLENAME = "vistorias";

        public static final String CONTENT_URI_PATH = TABLENAME;

        public static final String MIMETYPE_TYPE = TABLENAME;
        public static final String MIMETYPE_NAME = AUTHORITY + ".provider";

        // field info
        public static final String DATAVISTORIA = "dataVistoria";
        public static final String STATUSOBRA = "statusObra";
        public static final String OBRA = "obra_id";
        public static final String VALORMEDICAO = "valorMedicao";
        public static final String COMENTARIOS = "comentarios";
        public static final String PLANILHA = "planilha";
        public static final String PROJETOS = "projetos";
        public static final String ESPECIFICACOES = "especificacoes";
        public static final String DIARIO = "diario";
        public static final String CONCLUIDA = "concluida";
        public static final String INICIO = "inicio";
        public static final String FIM = "fim";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String UPDATED = "updatedAt";

        // content uri pattern code
        public static final int CONTENT_URI_PATTERN_MANY = 5;
        public static final int CONTENT_URI_PATTERN_ONE = 6;

        // Refer to activity.
        public static final Uri CONTENT_URI = new Uri.Builder()
            .scheme(ContentResolver.SCHEME_CONTENT)
            .authority(AUTHORITY)
            .appendPath(CONTENT_URI_PATH)
            .build();
    }
    
    public static class RealizadoContract implements BaseColumns
    {
        public static final String TABLENAME = "realizado";

        public static final String CONTENT_URI_PATH = TABLENAME;

        public static final String MIMETYPE_TYPE = TABLENAME;
        public static final String MIMETYPE_NAME = AUTHORITY + ".provider";

        // field info
        public static final String DESCRICAO = "descricao";
        public static final String VALOR = "valor";
        public static final String VISTORIA = "vistoria_id";
        public static final String AVALIACAO = "avaliacao";
        public static final String COMENTARIOS = "comentarios";
        public static final String UPDATED = "updatedAt";

        // content uri pattern code
        public static final int CONTENT_URI_PATTERN_MANY = 7;
        public static final int CONTENT_URI_PATTERN_ONE = 8;

        // Refer to activity.
        public static final Uri CONTENT_URI = new Uri.Builder()
            .scheme(ContentResolver.SCHEME_CONTENT)
            .authority(AUTHORITY)
            .appendPath(CONTENT_URI_PATH)
            .build();
    }
    
    public static class FotoContract implements BaseColumns
    {
        public static final String TABLENAME = "foto";

        public static final String CONTENT_URI_PATH = TABLENAME;

        public static final String MIMETYPE_TYPE = TABLENAME;
        public static final String MIMETYPE_NAME = AUTHORITY + ".provider";

        // field info
        public static final String VISTORIA = "vistoria_id";
        public static final String URI = "uri";
        public static final String COMENTARIO = "comentario";
        public static final String UPDATED = "updatedAt";

        // content uri pattern code
        public static final int CONTENT_URI_PATTERN_MANY = 9;
        public static final int CONTENT_URI_PATTERN_ONE = 10;

        // Refer to activity.
        public static final Uri CONTENT_URI = new Uri.Builder()
            .scheme(ContentResolver.SCHEME_CONTENT)
            .authority(AUTHORITY)
            .appendPath(CONTENT_URI_PATH)
            .build();
    }

}
