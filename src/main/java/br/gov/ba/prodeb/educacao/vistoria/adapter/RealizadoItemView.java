package br.gov.ba.prodeb.educacao.vistoria.adapter;

import br.gov.ba.prodeb.educacao.vistoria.R;
import br.gov.ba.prodeb.educacao.vistoria.VistoriaAtual;
import br.gov.ba.prodeb.educacao.vistoria.model.Realizado;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

@EViewGroup(R.layout.vistoria_detail_list_item)
public class RealizadoItemView extends RelativeLayout {
	
	@ViewById
	TextView servico;
	
	@ViewById
	TextView previsto;

	@ViewById
	TextView anterior;

	@ViewById
	EditText executado;

	@ViewById
	EditText comentarios;
	
	@ViewById
	Spinner avaliacao;
	
	@Bean
	VistoriaAtual vistoriaAtual;
	
	TextWatcher executadoTW;
	TextWatcher comentariosTW;

	public RealizadoItemView(Context context) {
		super(context);
	}

    public void bind(Realizado realizado, final int position) {
    	
    	if (executadoTW != null) {
    		executado.removeTextChangedListener(executadoTW);
    	}
    	if (comentariosTW != null) {
    		comentarios.removeTextChangedListener(comentariosTW);
    	}
    	
    	executado.setSelectAllOnFocus(true);
    	
        servico.setText(realizado.getDescricao().toUpperCase());
        previsto.setText("(" + String.valueOf(realizado.getPrevisto()) + " % prev)");
        if (realizado.getValorAnterior() > 0) {
        	anterior.setText("(" + String.valueOf(realizado.getValorAnterior()) + " % ant)");
        } else {
        	anterior.setText("");        	
        }
        if (realizado.getValor() > 0) {
        	executado.setText(String.valueOf(realizado.getValor()));
        } else {
        	executado.setText("");        	
        }
        
//        executado.setHint(String.valueOf(realizado.getValorAnterior()));
        if (realizado.getComentarios() != null) {
        	comentarios.setText(realizado.getComentarios());
        } else {
        	comentarios.setText("");
        }

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
		        R.array.avaliacao, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		avaliacao.setAdapter(adapter);
		
		int pos=adapter.getPosition(realizado.getAvaliacao());
		avaliacao.setSelection(pos);

		executadoTW = new TextWatcher(){
	        public void afterTextChanged(Editable s) {
				try {
//                	valores.set(position, Float.parseFloat(realizadoItemView.executado.getText().toString()));
                	vistoriaAtual.realizado.set(position, Float.parseFloat(executado.getText().toString()));
				} catch (Exception e) {
                	vistoriaAtual.realizado.set(position, 0f);
				}			    		

	        }
	        public void beforeTextChanged(CharSequence s, int start, int count, int after){}
	        public void onTextChanged(CharSequence s, int start, int before, int count){
	        }
	    };
        executado.addTextChangedListener(executadoTW); 
        /*
        executado.setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus){
//                    final int position = v.getId();
                    final EditText valor = (EditText) v;
                    try {
//                    	valores.set(position, Float.parseFloat(valor.getText().toString()));
                    	vistoriaAtual.realizado.set(position, Float.parseFloat(valor.getText().toString()));
                    }
                    catch(NumberFormatException ex) {
//                    	valores.set(position, 0f);
                    	vistoriaAtual.realizado.set(position, 0f);
                    }
                }
            }
        });

        */
        
        comentariosTW = new TextWatcher(){
	        public void afterTextChanged(Editable s) {
				try {
                	vistoriaAtual.comentario.set(position, comentarios.getText().toString());
				} catch (Exception e) {
                	vistoriaAtual.comentario.set(position, "");
				}			    		

	        }
	        public void beforeTextChanged(CharSequence s, int start, int count, int after){}
	        public void onTextChanged(CharSequence s, int start, int before, int count){
	        }
	    };
            comentarios.addTextChangedListener(comentariosTW); 
/*
		comentarios.setOnFocusChangeListener(new OnFocusChangeListener() {
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus){
//                final int position = v.getId();
                final EditText comentarios = (EditText) v;
                try {
                	vistoriaAtual.comentario.set(position, comentarios.getText().toString());
                }
                catch(Exception ex) {
//                	vistoriaAtual.comentario.set(position, "");
                }
            }
        }
        
    });
*/
        avaliacao.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int pos, long id) {
		    	vistoriaAtual.avaliacao.set(position, avaliacao.getItemAtPosition(avaliacao.getSelectedItemPosition()).toString());
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) {
		        // your code here
		    }

		});

    }
    
    
}
