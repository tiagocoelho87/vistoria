package br.gov.ba.prodeb.educacao.vistoria;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import br.gov.ba.prodeb.educacao.vistoria.adapter.PrevistoAdapter;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.ObraContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.PrevistoContract;
import br.gov.ba.prodeb.educacao.vistoria.contentprovider.Contract.VistoriaContract;
import br.gov.ba.prodeb.educacao.vistoria.model.Obra;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

@EFragment(R.layout.relatorio_detail_fragment)
public class RelatorioDetailFragment extends Fragment {
	
	@ViewById
	TextView codObra;
		
	@App
	VistoriaApplication vistoriaApp;
	    
    @AfterViews
    public void configure() {
  			Cursor c =  this.getActivity().getContentResolver().query(Uri.parse(ObraContract.CONTENT_URI + "/" + vistoriaApp.getObraId()), null, null, null, null);
  			c.moveToNext();
  			Obra obra = Obra.fromCursor(getActivity(), c);
  			c.close();
  			getActivity().getActionBar().setSubtitle(obra.getCodigo());
  			codObra.setText(obra.getCodigo());
  			c.close();
      }


    @Override
    public void onResume() {
      super.onResume();
    }

}
